# TEST.MD

This is a test file for Adobe Markdown Extension .

Here is some italic text:

This is a sentence that has the word __italic__ in it.  To test, click the word italic above and do a ctrl-i or select the italic option from the popup menu.  It should add or remove the _ markers surrounding the text regardless of whether anything is selected.

Here is some bold text:

This is a sentence that has the word bold in it. To test, click the word **bold** above and do a ctrl-b or select the bold option from the popup menu.  It should add or remove the ** markers surrounding the text regardless of whether anything is selected.

>[!IMPORTANT]
>
>   This is an important alert.

>[!WARNING]
>
> This is a warning alert

>[!CAUTION]
>
> This is a caution alert.

>[!CAUTION]
>
> This is another caution alert.

Here are some non-printing characters that should trigger some problems:

BEL is 0007 

 

�

