---
title: "Components"
description: "Components are the fundamental authoring building block of content pages in AEM"
sub-product: "Authoring Content in AEM as a Cloud Service"
user-guide-title: "Authoring Content in AEM as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/sites-cloud/authoring/fundamentals/components.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/sites-cloud/authoring/fundamentals/components.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/sites-cloud/authoring/fundamentals/components.md"
---
# Components {#components}

Components are the fundamental authoring building block of content pages in Adobe Experience Manager (AEM). Components such as images, text, titles, etc. can be easily dragged and dropped to build your content.

AEM comes with a variety of out-of-the-box components that provide comprehensive functionality for website authors. They are available when [editing a page](/help/sites/authoring/fundamentals/editing-content.md) and are grouped by main functional area (called component group) to aid filtering.

>[!NOTE]
>
>This section only discusses components that are available out-of-the-box in a standard AEM installation.
>
>Depending on your instance you may have customized components developed explicitly for your requirements. These may even have the same name as some of the components discussed here.

## General Usage {#general-usage}

The components are available on the **Components** tab of the side panel of the page editor when [editing a page](/help/sites/authoring/fundamentals/editing-content.md).

You can select a component and drag it to the required location on your page. You can then edit it using:

* [Configure Properties](/help/sites/authoring/fundamentals/page-properties.md)
* [Edit Content](/help/sites/authoring/fundamentals/editing-content.md)
* [Edit Content - Full Screen Mode](/help/sites/authoring/fundamentals/editing-content.md#edit-content-full-screen-mode)

For further information about adding components to a page see the article [Editing Page Content](/help/sites/authoring/fundamentals/editing-content.md).

## Overview of All Components {#overview-of-all-components}

The [Components Console](/help/sites/authoring/features/components-console.md) gives an overview of the component groups and components that are provided by your AEM installation. You can view key information about the individual components and their usage.

## Core Components {#core-components}

The [Core Components](https://experienceleague.adobe.com/docs/experience-manager-core-components/using/introduction.html) offer flexible and feature-rich authoring functionality, which provide essential content types to create pages.

They are build using AEM best practices and are the foundation of the AEM authoring experience. Core Components can be easily extended by developers to meet specific project needs.

### Configuring Templates {#configuring-templates}

If your page is based on the recommended, modern, editable template, you can [edit the template](/help/sites/authoring/features/templates.md) enable/disable these and edit parameters for specific components.

