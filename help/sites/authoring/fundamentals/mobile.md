---
title: "Authoring a Page for Mobile Devices"
description: "When authoring for mobile, you can switch between several emulators to see what the end-user sees"
sub-product: "Authoring Content in AEM as a Cloud Service"
user-guide-title: "Authoring Content in AEM as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/sites-cloud/authoring/fundamentals/mobile.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/sites-cloud/authoring/fundamentals/mobile.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/sites-cloud/authoring/fundamentals/mobile.md"
---
# Authoring a Page for Mobile Devices {#authoring-a-page-for-mobile-devices}

Adobe Experience Manager pages are based on a responsive layout. Responsive layout adapts your content to fit the target device automatically, eliminating the need to author content for specific devices.

When authoring a mobile page, the page is displayed in a way that emulates the mobile device. When authoring the page, you can switch between several emulators to see what the end-user sees when accessing the page.

Devices are grouped into the categories feature, smart, and touch according to the capabilities of the devices to render a page. When the end-user accesses a mobile page, AEM detects the device and sends the representation that corresponds to its device group.

>[!NOTE]
>
>To create a mobile site based on an existing standard site, create a live copy of the standard site. See Creating a Live Copy for Different Channels.
>
>AEM developers can create new device groups. See Creating Device Group Filters.

<!--
>To create a mobile site based on an existing standard site, create a live copy of the standard site. (See [Creating a Live Copy for Different Channels](/help/sites-administering/msm-livecopy.md).)
>
>AEM developers can create new device groups. (See [Creating Device Group Filters](/help/sites-developing/groupfilters.md).)
-->

Use the following procedure to author a mobile page:

1. From global navigation open the **Sites** console.
1. Edit a content page.
1. Switch to the desired emulator by clicking the **Emulator** icon at the top of the page.

   ![Emulator icon](/help/assets/emulator.png)

1. Drag and drop components from the component browser or asset browser on to the page.
1. [Modify the responsive layout](/help/sites/authoring/features/responsive-layout.md) of the page and its components based on the selected device.

The page will look similar to the following:

![Mobile example](/help/assets/mobile.png)

>[!NOTE]
>
>The emulators are disabled when a page on the author instance is requested from a mobile device.

