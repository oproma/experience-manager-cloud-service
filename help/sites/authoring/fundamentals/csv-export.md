---
title: "Export to CSV"
description: "Export information about your pages to a CSV file on your local system"
sub-product: "Authoring Content in AEM as a Cloud Service"
user-guide-title: "Authoring Content in AEM as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/sites-cloud/authoring/fundamentals/csv-export.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/sites-cloud/authoring/fundamentals/csv-export.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/sites-cloud/authoring/fundamentals/csv-export.md"
---
# Export to CSV {#export-to-csv}

**Create CSV Report** enables you to export information about your pages to a CSV file on your local system.

* The file downloaded is called `export.csv`
* The contents are dependent on the properties you select.
* You can define the path together with the depth of the export.

>[!NOTE]
>
>The download feature and default destination of your browser is used.

The **Create CSV Export** wizard allows you to select:

* Properties to export
  * Metadata
    * Name
    * Modified
    * Published
    * Template
    * Workflow
  * Translation
    * Translated
  * Analytics
    * Page Views
    * Unique Visitors
    * Time on Page
* Depth
  * Parent Path
  * Direct children only
  * Additional levels of children
  * Levels

The resulting `export.csv` file can be opened in Excel or any other compatible application.

To create a CSV export:

1. Open the **Sites** console, navigate to the required location if required.
   * The create **CSV Report** option is available when browsing the **Sites** console (in List view)
   * It is an option of the **Create** drop down menu:

     ![Create CSV option](/help/assets/csv-create.png)

1. From the toolbar, select **Create** then **CSV Report** to open the wizard:

   ![CSV export options](/help/assets/csv-options.png)

1. Select the required properties to export.
1. Select **Create**.
   ![Resulting CSV export in Excel](/help/assets/csv-example.png)

