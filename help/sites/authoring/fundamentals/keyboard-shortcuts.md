---
title: "Keyboard Shortcuts for Page Editing"
description: "Various keyboard shortcuts are available throughout AEM, including some for page editing"
sub-product: "Authoring Content in AEM as a Cloud Service"
user-guide-title: "Authoring Content in AEM as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/sites-cloud/authoring/fundamentals/keyboard-shortcuts.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/sites-cloud/authoring/fundamentals/keyboard-shortcuts.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/sites-cloud/authoring/fundamentals/keyboard-shortcuts.md"
---
# Keyboard Shortcuts for Page Editing {#keyboard-shortcuts-when-editing-pages}

Various keyboard shortcuts are available throughout AEM. Some apply when editing pages, others to the [use of consoles](/help/sites/authoring/getting-started/keyboard-shortcuts.md).

>[!NOTE]
>
>The [modifier keys](#os-specific-modifier-keys) required for AEM keyboard shortcuts vary depending on operating system.

## Editing Keyboard Shortcuts {#editing-keyboard-shortcuts}

|Location|Shortcut|Description|
|---|---|---|
|Any edit window mode|`Ctrl-Shift-m`|Toggle between **Preview** and the currently selected [mode](/help/sites/authoring/fundamentals/environment-tools.md#page-modes)</a> (e.g. **Edit**, **Layout**, etc)*|
|**Edit** mode|`Ctrl-z`|[Undo last change](/help/sites/authoring/fundamentals/editing-content.md#undoing-and-redoing-page-edits)|
||`Ctrl-y`|[Redo last change](/help/sites/authoring/fundamentals/editing-content.md#undoing-and-redoing-page-edits)|
||`Ctrl-Click`|Select multiple paragraphs|
||`Ctrl-c`|Copy selected paragraph(s)|
||`Ctrl-x`|Cut selected paragraph(s), (cut paragraphs will not disappear in the UI until it is pasted)|
||`Ctrl-v`|Paste paragraph(s) previously cut or copied|
||`Ctrl-Backspace`|Delete selected paragraph(s)|
|**Edit** mode - Rich Text Editor|`Ctrl-b`|Bold|
||`Ctrl-I`|Italic|
||`Ctrl-u`|Underline|

>[!NOTE]
>
>Once the user starts to interact with the page in Preview Mode, the `Ctrl-Shift-m` shortcut is no longer available. The user must click on the top editor bar or go back to the Edit mode for the shortcut to once again become available.

Various keyboard shortcuts are also available for desktop users when using the [consoles](/help/sites/authoring/getting-started/keyboard-shortcuts.md).

>[!NOTE]
>
>Editor keyboard shortcuts are always active regardless if the [console shortcuts have been deactivated](/help/sites/authoring/getting-started/keyboard-shortcuts.md#deactivating-keyboard-shortcuts).

## OS-Specific Modifier Keys {#os-specific-modifier-keys}

The modifier keys used for the keyboard shortcuts vary depending on the operating system used by the client.

|Windows and Linux|macOS|
|---|---|
|`Ctrl`|`Command`|
|`Alt`|`Option`|

