---
title: "Troubleshooting AEM when Authoring"
description: "Some issues that you might encounter when using AEM"
sub-product: "Authoring Content in AEM as a Cloud Service"
user-guide-title: "Authoring Content in AEM as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/sites-cloud/authoring/fundamentals/troubleshooting.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/sites-cloud/authoring/fundamentals/troubleshooting.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/sites-cloud/authoring/fundamentals/troubleshooting.md"
---
# Troubleshooting AEM when Authoring {#troubleshooting-aem-when-authoring}

The following section covers some issues that you might encounter when using AEM, together with suggestions on how to troubleshoot them.

## Old page version still on published site {#old-page-version-still-on-published-site}

* **Issue**:
  * You have made changes to a page and published the page to the publish site, but the *old* version of the page is still being shown on the publish site.
* **Reason**:
  * This can have several causes, most often the cache (either your local browser or the Dispatcher), though it can sometimes be an issue with the replication queue.
* **Solutions**:
  * There are various possibilities here:
  * Confirm that the page has been replicated correctly. Check the page status and if necessary, the state of the replication queue.
  * Clear the cache in your local browser and access your page again.
  * Add `?` to the end of the page URL. For example:
    * `http://<host>:<port>/sites.html/content?`
    * This will request the page directly from AEM and bypass the Dispatcher. If you receive the updated page, it is an indication that you should clear the Dispatcher cache.
  * Contact your system administrator is there are issues with the replication queues.

## Component Actions not visible on Toolbar {#component-actions-not-visible-on-toolbar}

* **Issue**:
  * The full range of applicable component actions are not visible when editing a content page on the author environment.
* **Reason**:
  * In rare cases a previous action might impact the toolbar.
* **Solution**:
  * Refresh the page.

