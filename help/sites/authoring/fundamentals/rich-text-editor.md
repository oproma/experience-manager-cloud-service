---
title: "Using the Rich Text Editor to Author Content"
description: "Using the Rich Text Editor to Author Content"
sub-product: "Authoring Content in AEM as a Cloud Service"
user-guide-title: "Authoring Content in AEM as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/sites-cloud/authoring/fundamentals/rich-text-editor.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/sites-cloud/authoring/fundamentals/rich-text-editor.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/sites-cloud/authoring/fundamentals/rich-text-editor.md"
---
# Using the Rich Text Editor to Author Content {#use-rich-text-editor-to-author-content}

The Rich Text Editor (RTE) is a basic building block for inserting textual content into AEM. It forms the basis of various components

## In-Place Editing {#in-place-editing}

Selecting a text-based component with a single tap or click will reveal the [component toolbar](/help/sites/authoring/fundamentals/editing-content.md#component-toolbar) as with any component.

![The component toolbar](/help/assets/editing-component-toolbar.png)

Tapping/clicking agin or initially selecting the component with a slow double-tap/click will open in-place editing, which has its own toolbar. Here you can edit the content and make basic formatting changes.

![In place editing with the RTE](/help/assets/rte-in-place-editing.png)

This toolbar provides the following options:

* **Format**: This allows you to set Bold, Italic and Underline.
* **Lists**: With this you can create bulleted or numbered lists, or set the indentation.
* **Hyperlink**
* **Unlink**
* **Full Screen**
* **Close**
* **Save**

## Full Screen Editing {#full-screen-editing}

For text-based components, tapping the full screen mode from the [toolbar](/help/sites/authoring/fundamentals/editing-content.md#component-toolbar) opens the rich text editor and hides the rest of the page content.

![RTE full screen button](/help/assets/editing-full-screen.png)

Full screen mode displays all the configured options that you can use for authoring. The availability of options depends on the configuration. <!--Full screen mode displays all the configured options that you can use for authoring. The availability of options [depends on the configuration](/help/sites-administering/rich-text-editor.md).-->

![RTE in full screen mode](/help/assets/rte-full-screen.png)

Additional rich text editor options include:

* **Anchor**: Create an anchor in the text that you can later link to/reference.
* **Align Text Left**
* **Center Text**
* **Align Text Right**

Close full screen mode by clicking the minimize icon.

![RTE minimize button](/help/assets/rte-minimize.png)

>[!NOTE]
>
>Copying nested lists from Microsoft Word into the RTE can give inconsistent results and may require manual adjustment after pasting the text in the RTE.

