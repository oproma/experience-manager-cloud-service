---
title: "Keyboard Shortcuts for Consoles"
description: "Save time when authoring by using these keyboard shortcuts"
sub-product: "Authoring Content in AEM as a Cloud Service"
user-guide-title: "Authoring Content in AEM as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/sites-cloud/authoring/getting-started/keyboard-shortcuts.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/sites-cloud/authoring/getting-started/keyboard-shortcuts.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/sites-cloud/authoring/getting-started/keyboard-shortcuts.md"
---
# Keyboard Shortcuts for Consoles {#keyboard-shortcuts-for-consoles}

The following shortcuts are available for the Sites, Assets, Users, Groups, Projects, and Experience Fragments consoles.

|Location|Shortcut|Description|
|---|---|---|
|General|`Ctrl+Click`|Multi-select|
|Console - Column View & Content View|`Right Arrow`|Navigate down into the structure of your website / expand tree node|
||`Left Arrow`|Navigate up the tree structure / collapse tree node|
||`Down Arrow`|Move down a list of pages at the same level|
||`Up Arrow`|Move up a list of pages at the same level|
|Console - Column View|`Shift-Up/Down Arrow`|Move up a list of pages at the same level|
||`Esc`|Move up a list of pages at the same level|
|Consoles|`?`|Keyboard shortcut help|
||`/`|Invoke [search](/help/sites/authoring/getting-started/search.md)|
||`Alt+`&lt;`Number`&gt;|Toggle which [rail selector](/help/sites/authoring/getting-started/basic-handling.md#rail-selector) option to view|
||&#96;|Hide rail|
||`e`|Edit|
||`p`|Properties|
||`m`|Move|
||`Ctrl+c`|Copy|
||`Ctrl+v`|Paste|
||`Backspace`|Delete|
|Property Pages|`Ctrl+s`|Save|

The key combinations are listed with the Windows [modifier keys](#os-specific-modifier-keys).

## OS-Specific Modifier Keys {#os-specific-modifier-keys}

The modifier keys used for the keyboard shortcuts vary depending on the operating system used by the client.

|Windows/Linux|macOS|
|---|---|
|`Alt`|`Option`|
|`Ctrl`|`Command`|

## Browsers and Keyboard Shortcuts {#browsers-and-keyboard-shortcuts}

AEM avoids common shortcuts already used by popular browsers, however default browser behavior is not overridden by AEM shortcuts.

In addition keyboard shortcuts are the same for all locales and keyboard layouts.

>[!NOTE]
>
>AEM shortcuts do not override default browser behavior.
>
>In the case that conflicts should occur between AEM and browser shortcuts, AEM shortcuts will not function, yielding to browser functionality.

>[!CAUTION]
>
>AEM keyboard shortcuts could interfere with screen readers, some browser features, and other accessibility tools.
>
>Adobe recommends [disabling AEM keyboard shortcuts](#deactivating-keyboard-shortcuts) when using a screen reader.

## Deactivating Keyboard Shortcuts {#deactivating-keyboard-shortcuts}

AEM provides keyboard shortcuts for some of the commonly-used features (edit, properties, copy/paste, showing various side-rail menus, etc.).

For users that do not use keyboard shortcuts or have certain accessibility requirements, all keyboard shortcuts can be disabled.

You can deactivate keyboard shortcuts in two locations:

* [My Preferences](/help/sites/authoring/getting-started/account-environment.md#my-preferences)
* The [help menu](/help/sites/authoring/getting-started/basic-handling.md#accessing-help)

>[!NOTE]
>
>The navigation shortcut keys for the column view and content tree are always active.

## Keyboard Shortcuts for the Page Editor {#keyboard-shortcuts-for-the-page-editor}

Various keyboard shortcuts are available throughout AEM. Some apply to the use of consoles, others to [page editing](/help/sites/authoring/fundamentals/keyboard-shortcuts.md).

