---
title: "Working with Workflows"
description: "Workflows in AEM allow you to automate a series of steps that are performed on a page or asset."
sub-product: "Authoring Content in AEM as a Cloud Service"
user-guide-title: "Authoring Content in AEM as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/sites-cloud/authoring/workflows/overview.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/sites-cloud/authoring/workflows/overview.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/sites-cloud/authoring/workflows/overview.md"
---
# Working with Workflows {#working-with-workflows}

AEM Workflows allows you to automate a series of steps that are performed on (one or more) pages and/or assets.

For example, when publishing, an editor has to review the content - before a site administrator activates the page. A workflow that automates this example notifies each participant when it is time to perform their required work:

1. The author applies the workflow to the page.
1. The editor receives a work item that indicates that they are required to review the page content. When finished, they indicate that their work item is complete.
1. The site administrator then receives a work item that requests the activation of the page. When finished, they indicate that their work item is complete.

Typically:

* Content authors apply workflows to pages as well as participate in workflows.
* The workflows that you use are specific to the business processes of your organization.

The following pages cover:

* [Applying Workflows to Pages](/help/sites/authoring/workflows/applying.md)
* [Participating in Workflows](/help/sites/authoring/workflows/participating.md)

