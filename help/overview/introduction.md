---
title: "Introduction to Adobe Experience Manager as a Cloud Service"
description: "Introduction to Adobe Experience Manager (AEM) as a Cloud Service."
sub-product: "Overview of AEM as a Cloud Service"
user-guide-title: "Overview of AEM as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://experienceleague.adobe.com/docs/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/overview/introduction.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/overview/introduction.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/overview/introduction.md"
---
# An Introduction to Adobe Experience Manager as a Cloud Service {#an-introduction-to-adobe-experience-manager-as-a-cloud-service}

[Adobe Experience Manager](https://www.adobe.com/marketing/experience-manager.html) (AEM) is now available as a Cloud Service.

Adobe Experience Manager as a Cloud Service:

* Is the cloud-native way of leveraging the AEM applications.

* Enables you to provide your customers with personalized, content-led experiences, by combining the power of the AEM Content Management System with AEM Digital Asset Management.

* Provides Continuous Delivery and Continuous Integration for updates with zero downtime.

* Delivers content quickly and efficiently, leveraging a built-in Content Delivery Network (CDN) and other network layer best practices.

* Is based on a dynamic architecture that autoscales, thus removing infrastructure considerations.

* Is security focused, using automated tests to scan for common vulnerabilities.

* Validates customer code using automated tests.

* Has performance topologies optimized for maximum resilience and efficiency.

* Significantly reduces the manual configuration required.

* Is deeply integrated with the Adobe Experience Cloud.

* Introduces the next generation of the Adobe Experience Manager product line, building on past investments and innovations, preserving and extending all use cases and functionalities.

All these help:

* Developers concentrate on configuring and extending AEM, following the same patterns that have made AEM successful so far augmented with the new cloud-based development pattern.

* Authors enjoy the latest innovations, delivered to them regularly.

* Simplify configuration and infrastructure for the system administrators.

* Marketing professionals experience quicker time-to-value.

>[!NOTE]
>
>See [Terminology](/help/overview/terminology.md) for some of the new terms that have been introduced with AEM as a Cloud Service.

## Value Added as a Cloud Service {#value-added-as-a-cloud-service}

AEM achieves these goals by adopting the main characteristics of modern cloud services:

* It is always on:

  * The entire service architecture has been revisited so that you do not experience any downtime; for neither the content management, nor the content delivery capabilities.

* It is always at scale:

  * All instances of AEM as a Cloud Service are created equal; so the service architecture will automatically scale, up and down, depending on your needs.

* It is always current:

  * AEM as a Cloud Service implements a new, continuous delivery pipeline for the AEM codebase, with automated updates up to several times a month. This solves one of the main challenges of AEM applications, by keeping you current on the most recent version.

* It is always evolving:

  * AEM as a Cloud Service evolves on a daily basis, based on the projects implemented by our customers. Content, code and configurations are constantly reviewed and vetted against best practices, allowing us to guide you on how to achieve your business goals.

## Getting to Know Adobe Experience Manager as a Cloud Service {#getting-to-know-aem-as-cloud-service}

* [What is New and What is Different](/help/overview/what-is-new-and-different.md) between Adobe Experience Manager as a Cloud Service and previous versions
* The [Architecture](/help/core-concepts/architecture.md) of Adobe Experience Manager as a Cloud Service
* [Notable Changes to AEM Sites in AEM as a Cloud Service](/help/sites/sites-cloud-changes.md)
* [Introducing Assets as a Cloud Service](/help/assets/overview.md)
* [Adobe Experience Manager as a Cloud Service Tutorials](https://experienceleague.adobe.com/docs/experience-manager-learn/cloud-service/overview.html)

