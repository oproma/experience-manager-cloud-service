---
title: "Known Issues"
description: "Release notes specific to the Known Issues with Adobe Experience Manager as a Cloud Service"
sub-product: "Release Notes for AEM as a Cloud Service"
user-guide-title: "Release Notes for AEM as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/release-notes/known-issues.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/release-notes/known-issues.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/release-notes/known-issues.md"
---
# Known issues {#known-issues}

This article lists the known issues of Adobe Experience Manager as a Cloud Service offering. The list is revised and updated with each continuous release of Experience Manager.

[Contact support](https://helpx.adobe.com/support/experience-manager.html) for more information about the known issues.

<!-- 
## Platform {#platform}

## Sites {#sites}
-->

## Assets {#assets}

<!-- Jira label: assets-cloud-known-issues -->

Some known issues are:

* **Metadata Schema**: Asset rating widget used to cause JSP compilation error. It was removed from the metadata schema. <!-- CQ-4282865, CQ-4284633 -->

### Upcoming Assets capabilities {#upcoming-assets-capabilities}

A few capabilities of Adobe Experience Manager Assets that depend on foundation capabilities, which are not yet available in the Experience Manager as a Cloud Service deployment architecture, are expected to be enabled at a later stage:

* Enhanced smart tagging functionality that leverages AI services of Adobe I/O are not available for now.
* Capabilities not enabled at this stage due to dependency on Commerce Integration Framework APIs:
  * Photoshoot workflow models.
  * Product information tab in the asset properties user interface is not populated.
* Capabilities not enabled at this stage due to dependency on InDesign Server integration:
  * Asset Templates and Asset Catalogs.
  * Multi-page preview of InDesign files.

>[!MORELIKETHIS]
>
>* [Major changes in AEM](/help/release-notes/aem-cloud-changes.md)
>* [Deprecated and removed features](/help/release-notes/deprecated-removed-features.md)
>* [Release notes](/help/release-notes/home.md)

