---
title: "Release Notes for Release 2020.2.0"
description: "Release Notes for Release 2020.2.0"
sub-product: "Release Notes for AEM as a Cloud Service"
user-guide-title: "Release Notes for AEM as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/release-notes/release-notes-cloud/release-notes-2020-2-0.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/release-notes/release-notes-cloud/release-notes-2020-2-0.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/release-notes/release-notes-cloud/release-notes-2020-2-0.md"
---
# Release Notes for AEM as a Cloud Service 2020.2.0 {#release-notes}

The following section outlines the general Release Notes for Experience Manager as a Cloud Service 2020.2.0.

## Release Date {#release-date}

The Release Date for Experience Manager as a Cloud Service 2020.2.0 is February 13, 2020.

## Cloud Manager {#cloud-manager}

Follow this section to learn about what is new and the updates for Cloud Manager in AEM as a Cloud Service Release 2020.2.0.

### What's New {#what-is-new}

* The Adobe Experience Manager archetype version has been updated to version 22.
* The Stage and Production environments in Sandbox/Demo programs can now be updated through the Cloud Manager UI.
* URLs used in Experience Cloud notifications were optimized to avoid an extra redirect.
* Pipeline execution steps which timed out now explicitly state this.
* The Code Scanning step now has a downloadable log.
* The spreadsheet containing issues discovered during code scanning now has a column with a link to documentation for the specific rule.

### Bug Fixes  {#bug-fixes}

* Browser security policies would sometimes prevent certain buttons in the pipeline execution screen from working properly.
* The Overview, Environments, and Activity links were sometimes available on the Cloud Manager landing page.
* Certain failures when deploying could erroneously prevent new pipelines from being created.

