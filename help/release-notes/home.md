---
title: "Release Notes for Adobe Experience Manager (AEM) as a Cloud Service."
description: "Release Notes for Adobe Experience Manager (AEM) as a Cloud Service."
sub-product: "Release Notes for AEM as a Cloud Service"
user-guide-title: "Release Notes for AEM as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://experienceleague.adobe.com/docs/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/release-notes/home.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/release-notes/home.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/release-notes/home.md"
---
# Release Information {#release-information}

| Product | Adobe Experience Manager as a Cloud Service |
|---|---|
| Version | 2020.04.0 |
| Type | Continuous Update |
| Availability date | Continuous Update |

## Key Release Information {#key-articles}

* [Current Release Notes](/help/release-notes/release-notes/release-notes-current.md)
* [What is New](/help/release-notes/what-is-new.md)
* [Notable Changes](/help/release-notes/aem-cloud-changes.md)
* [Known Issues](/help/release-notes/known-issues.md)
* [Deprecated and Removed Features](/help/release-notes/deprecated-removed-features.md)

## Experience Manager as a Cloud Service Guides {#aem-guides}

|User Guide|Description|
|---|---|
|[Experience Manager as a Cloud Service Home](/help/landing/home.md)|For an overview of Experience Manager as a Cloud Service documentation, start here.|
|[Overview](/help/overview/home.md)|This guide provides an overview of Experience Manager as a Cloud service, including an introduction, terminology, etc.|
|[Release Notes](/help/release-notes/home.md)|This guide provides important information about the latest release of Experience Manager as a Cloud Service, including what's new deprecated and removed features, and known issues.|
|[Core Concepts](/help/core-concepts/home.md)|This guide provides an introduction to the core concepts of Experience Manager as a Cloud Service, including the architecture of the new service.|
|[Security User Guide](/help/security/home.md)|Learn about important security topics regarding Experience Manager as a Cloud Service.|
|[Onboarding](/help/onboarding/home.md)|This guide provides an summary of how to get started with Experience Manager as a Cloud Service, including how to get access and important data protection information.|
|[Sites User Guide](/help/sites/home.md)|Understand how to administer Experience Manager Sites as a Cloud Service.|
|[Assets User Guide](/help/assets/home.md)|Understand how to administer Experience Manager Assets as a Cloud Service.|
|[Implementing User Guide](/help/implementing/home.md)|Learn how to customize your Experience Manager as a Cloud Service deployment including development deployment topics.|
|[Connectors User Guide](/help/connectors/home.md)|Learn how to integrate solutions into Experience Manager as a Cloud Service.|
|[Operations User Guide](/help/operations/home.md)|Learn about the back-end operations of Experience Manager as a Cloud Service such as indexing and maintenance tasks.|

## Other Experience Manager Resources {#other-resources}

* [Dispatcher Documentation](/help/implementing/dispatcher/overview.md)
* [HTL Documentation](https://experienceleague.adobe.com/docs/experience-manager-htl/using/overview.html)
* [Core Components Documentation](https://experienceleague.adobe.com/docs/experience-manager-core-components/using/introduction.html)
* [Cloud Manager Documentation](https://experienceleague.adobe.com/docs/experience-manager-cloud-manager/using/introduction-to-cloud-manager.html)
* [GDPR Readiness](/help/onboarding/data-privacy/aem-readiness.md)
* [Adobe Experience Manager as a Cloud Service Tutorials](https://experienceleague.adobe.com/docs/experience-manager-learn/cloud-service/overview.html)
* [Experience League](https://guided.adobe.com/?promoid=K42KVXHD&mv=other#solutions/experience-manager)
* [AEM Community Forum](https://forums.adobe.com/community/experience-cloud/marketing-cloud/experience-manager)

