---
title: "What is new?"
description: "What is new in Adobe Experience Manager (AEM) as a Cloud Service."
sub-product: "Release Notes for AEM as a Cloud Service"
user-guide-title: "Release Notes for AEM as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/release-notes/what-is-new.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/release-notes/what-is-new.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/release-notes/what-is-new.md"
---
# What is New? {#what-is-new}

<!-- For the pre-release of Adobe Experience Manager (AEM) as a Cloud Service everything is new. -->

AEM Cloud Service is a cloud-native platform that optimizes marketer and developer workflows for the entire content lifecycle, including web content and digital asset management capabilities. Being cloud-native, it has scalable performance, has secure infrastructure, gets agile updates to stay the latest, and allows for hassle-free deployments and administration.

For an overview of Cloud Service, see the [Introduction to AEM as a Cloud Service](/help/overview/introduction.md).

<!-- Please link to introduction or what's new of Sites. -->

For an overview of Assets, see [Introduction to Assets as a Cloud Service](/help/assets/overview.md)

If you are familiar with previous versions of AEM, see the following for an overview of the notable changes in AEM as a Cloud Service:

* [Notable changes to Experience Manager as a Cloud Service](/help/release-notes/aem-cloud-changes.md)
* [Notable changes to AEM Sites as a AEM Cloud Service](/help/sites/sites-cloud-changes.md)
* [Notable changes to AEM Assets as a Cloud Service](/help/assets/assets-cloud-changes.md)

