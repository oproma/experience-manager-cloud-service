---
title: "Accessing Git"
seo-title: "Accessing Git"
description: "This page describes how you can access and manage Git repository."
seo-description: "Follow this page to learn how to access and manage your Git repository."
sub-product: "Implementing for AEM as a Cloud Service"
user-guide-title: "Implementing for AEM as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/implementing/cloud-manager/accessing-git.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/implementing/cloud-manager/accessing-git.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/implementing/cloud-manager/accessing-git.md"
---
# Accessing Git {#accessing-git}

You can access and manage your Git Repository using Self-Service Git Account Management from Cloud Manager UI.

## Using Self-Service Git Account Management {#self-service-git}

Use the **Manage Git** button available from the Cloud Manager UI, most prominently on the pipeline card.

1. Navigate to your *Program's Overview* page and to Pipelines card.

1. You will view the **Manage Git** option to access and manage your Git Repository.

   ![](/help/assets/manage-git1.png)

   Additionally, if you select the **Non-Production** pipeline tab, you will view the **Manage Git** option there too.

   ![](/help/assets/manage-git-new2.png)

>[!NOTE]
>The **Manage Git** option is visible to users in the Developer or Deployment Manager role. Clicking on this button opens a dialog which allows the user to find the URL to their Cloud Manager Git Repository along with their username and password.

   ![](/help/assets/manage-git3.png)

The important considerations to manage your Git in Cloud Manager are:

* **URL**: The repository URL
* **Username**: The user name
* **Password**: The value shown when the **Generate Password** button is clicked.


>[!NOTE]
>
>A user can check out a copy of their code, and make changes in the local code repository. When ready, the user can commit their code changes back to the remote code repository in Cloud Manager.

