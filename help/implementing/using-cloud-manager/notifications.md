---
title: "Notifications - Cloud Service"
description: "Notifications - Cloud Service"
sub-product: "Implementing for AEM as a Cloud Service"
user-guide-title: "Implementing for AEM as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/implementing/cloud-manager/notifications.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/implementing/cloud-manager/notifications.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/implementing/cloud-manager/notifications.md"
---
# Understanding Notifications {#notifications} 

Cloud Manager allows the user to receive notifications when the production pipeline starts and completes (successfully or unsuccessfully), at the start of a production deployment. These notifications are sent through the Adobe Experience Cloud Notification system.

>[!NOTE]
>
>The approval and scheduled notifications are only sent to users in the Business Owner, Program Manager, and Deployment Manager roles.

The notifications appear in a sidebar in Cloud Manager UI (User Interface) and throughout the Adobe Experience Cloud.
![](/help/assets/notify-1.png)

Click on the bell icon from the header to open the sidebar and view the notifications, as shown in the figure below:

![](/help/assets/notify-2.png)

The sidebar lists the most recent notifications.


## Email Notifications {#email-notifications}

By default, notifications are available in the web user interface across Adobe Experience Cloud solutions. Individual users can also opt for these notifications to be sent through email, either on an immediate or digest basis.


This will take the user to the Notifications Preferences screen in Adobe Experience Cloud.

The users can turn on email notifications and (optionally) select the types of notifications they want to receive over email.

>[!NOTE]
>
>You can also enable digesting from the Adobe Experience Cloud, as shown below:

