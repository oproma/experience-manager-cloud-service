---
title: "Maintaining an AEM Connector"
description: "Maintaining an AEM Connector"
sub-product: "Connectors for AEM as a Cloud Service"
user-guide-title: "Connectors for AEM as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/connectors/maintain.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/connectors/maintain.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/connectors/maintain.md"
---
Maintaining an AEM Connector
============================

This article contains information about maintaining an AEM Connector and should be read in conjunction with articles about [implementing](/help/connectors/implement.md) and [submitting](/help/connectors/submit.md) connectors.

Even after the initial submission, there may be reasons for a partner to update its AEM Connector, either due to a new release of AEM or independent of that – for example, to add features or to fix bugs. This article outlines the process for both scenarios and also describes a customer's typical process for validating connectors when upgrading AEM. 

AEM as a Cloud Service applications are updated with AEM maintenance patches on a daily basis, with larger changes activated on a monthly basis during a feature release. While AEM updates are intended to be backwards compatible and thus not break applications, vendor partners are advised to regularly validate that their connectors behave as they would expect. Access to an AEM program/environment will be determined by the Partner Team.

