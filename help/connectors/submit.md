---
title: "Submitting an AEM Connector"
description: "Submitting an AEM Connector"
sub-product: "Connectors for AEM as a Cloud Service"
user-guide-title: "Connectors for AEM as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/connectors/submit.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/connectors/submit.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/connectors/submit.md"
---
Submitting an AEM Connector
===========================

Provided below is useful information for submitting AEM Connectors and should be read in conjunction with articles about [implementing](/help/connectors/implement.md) and  [maintaining](/help/connectors/maintain.md) connectors.

AEM Connectors are listed on the [Adobe Exchange](https://marketing.adobe.com/resources/content/resources/en/exchange/marketplace.html).

In previous AEM solutions, Package Manager was used to install connectors on various AEM instances. However, with AEM as a Cloud Service, connectors are deployed during the CI/CD process in Cloud Manager. In order for the connectors to be deployed, connectors need to be referenced in the maven project's pom.xml. 

There are various options of how the packages can be included in a project:

1. Partner's public repository - a partner would host the content package in a publicly accessible maven repository
1. Bundled artifact - in this case, the connector package is included locally in the customer's maven project.

Regardless of where they're hosted, packages need to be referenced as dependencies in the pom.xml, as provided by the vendor.

```xml
<!-- UberJAR Dependency to be added to the project's Reactor pom.xml -->
<dependency>
  <groupId>com.partnername</groupId>
  <artifactId>my-artifact</artifactId>
  <version>V123</version> <!-- use the latest! -->
  <scope>provided</scope>
  <classifier>my_classifier</classifier>
</dependency>
```

If the ISV partner hosts the connector on a internet accessible (such as Cloud Manager accessible) maven repository, the ISV should provide the repository configuration  where the pom.xml can be placed, so the connector dependencies (above) can be resolved at build time (both locally, and by Cloud Manager).

```xml
<repository>
    <id>the-repository</id>
    <name>The Repository Where the Connector is Hosted</name>
    <url>https://repo.partnername.com/repositories/aem_connector_repo</url>
    <releases>
        <enabled>true</enabled>
        <updatePolicy>never</updatePolicy>
    </releases>
    <snapshots>
        <enabled>false</enabled>
    </snapshots>
</repository>
```

If the ISV partner chooses to distribute the Connector as downloadable files, then the ISV should provide instructions on how the files can be deployed to a local-filesystem maven repository that needs to be checked into Git as part of the AEM project, so that Cloud Manager can resolve these dependencies.

