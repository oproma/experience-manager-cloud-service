---
title: "Navigation"
description: "Navigation - Cloud Service"
sub-product: "Onboarding to AEM as a Cloud Service"
user-guide-title: "Onboarding to AEM as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/onboarding/getting-access-to-aem-in-cloud/navigation.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/onboarding/getting-access-to-aem-in-cloud/navigation.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/onboarding/getting-access-to-aem-in-cloud/navigation.md"
---
# Accessing Experience Manager as a Cloud Service {#navigation} 

Once your System Administrator grants you access to Cloud Manager, you will receive an email that will take you to Cloud Manager login page which is also accessible through [Adobe Experience Cloud](https://my.cloudmanager.adobe.com/). 

Upon successful login, you will be directed to the landing page of Cloud Manager as shown below.

   ![](/help/assets/first_timelogin1.png)

## For Existing AMS Customers {#existing-aem}

If you are an existing AMS (Adobe Managed Services) customer and you have access to Cloud Service, you will see your existing program(s) and the **Add Program** button on the top right corner of the landing page. 

If you do not see the **Add Program** button and have questions about access to Cloud Service, please contact your Adobe representative.

Refer to [Adding a New Program in Cloud Service with Existing Programs](/help/onboarding/getting-access/first-time-login.md#existing-program) for more details.

## For New Cloud Service Customers {#new-cloud-services}

If you are a new Cloud Service customer, then you will see the **Add Program** button on the top right corner of an empty landing page. You will want to add a new program to your Cloud Service.

Refer to [Adding a New Program in Cloud Service with no Existing Programs](/help/onboarding/getting-access/first-time-login.md#no-program) for more details.

