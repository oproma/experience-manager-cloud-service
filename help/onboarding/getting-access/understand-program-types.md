---
title: "Understanding Program and Program Types"
description: "Understanding Program and Program Types - Cloud Services"
sub-product: "Onboarding to AEM as a Cloud Service"
user-guide-title: "Onboarding to AEM as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/onboarding/getting-access-to-aem-in-cloud/understand-program-types.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/onboarding/getting-access-to-aem-in-cloud/understand-program-types.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/onboarding/getting-access-to-aem-in-cloud/understand-program-types.md"
---
# Understanding Programs and Program Types {#understanding-programs} 

In Cloud Manager you have the Tenant entity at the very top which can have multiple Programs within it.  Each Program can contain no more than one Production environment, and multiple non-production environments. 

The following diagram shows the hierarchy of entities in Cloud Manager.

   ![image](/help/assets/program-types1.png)

## Program Types {#program-types}

A user can create a **Sandbox** or a **Regular** program. 

A *Sandbox* is typically created to serve the purposes of training, running demo’s, enablement, POC’s, or documentation. It is not meant to carry live traffic and will have restrictions that a regular program will not. It will include Sites and Assets and will be delivered auto-populated with a Git branch that includes sample code, a Dev environment, and a non-production pipeline.

A *Regular program* is created to enable live traffic at the appropriate time in the future.

