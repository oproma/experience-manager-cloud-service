---
title: "Creating a Program - Cloud Service"
description: "Creating a Program - Cloud Service"
sub-product: "Onboarding to AEM as a Cloud Service"
user-guide-title: "Onboarding to AEM as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/onboarding/getting-access-to-aem-in-cloud/creating-a-program.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/onboarding/getting-access-to-aem-in-cloud/creating-a-program.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/onboarding/getting-access-to-aem-in-cloud/creating-a-program.md"
---
# Creating a Program {#create-a-program}

The cloud-native solution provides the user with requisite permissions and the ability to create a program on a self-service model.

A program creation wizard will ask the user to submit details, depending on the user’s objective in creating the program within the bounds of what is available to the specific customer or organization.

In the event of first-time access to Cloud Manager or if no programs exist in the tenant, the user will see **Create your first Program** screen. If user select *Esc* or clicks out of the dialog box, the following screen displays:

 ![](/help/assets/create-program1.png)


## Using Create Program Wizard {#using-create-program-wizard}

Depending upon the user’s objective in creating the program within the bounds of what is available to the specific customer/organization, a program creation wizard will ask the user to submit one or more details.

  ![](/help/assets/create-program-2.png)

>[!NOTE]
>If a program already exists, then you will see **Add Program** on the top right of the landing page, as shown in the figure below.

![](/help/assets/create-program-add.png)

## Creating a Demo Program {#create-demo-program} 

>[!NOTE]
>A demo program is analogous to a sandbox program in Cloud Manager UI.

Follow the steps below to create a sandbox program: 
 
1. From the create program wizard, select **Set up a demo**. User submits program name before selecting **Create**.

   ![](/help/assets/create-program-setupdemo.png)

1. User will see the new sandbox program card on the landing page, and can hover over it to select the Cloud Manager icon to navigate to the Cloud Manager overview page. The card will inform the user on the status of auto-setup of the newly created sandbox program. User will see progression.

    ![](/help/assets/program-create-setupdemo2.png)

1. After the program set up and the project creation step completes, the user can access **Manage Git** link, as shown in the figure below:

   ![](/help/assets/create-program4.png)
   
   >[!NOTE]
   >
   >To learn more about accessing and managing your Git Repository using Self-Service Git Account Management from Cloud Manager UI, refer to [Accessing Git](/help/implementing/managing-code/accessing-git.md).


1. Once the development environment is created, the user can **Access AEM** link, as shown in the figure below:

   ![](/help/assets/create-program-5.png)

1. Once the Non-production pipeline deploying to development is complete, the wizard guides the user to either access AEM (on development) or to deploy code to development environment:

   ![](/help/assets/create-program-setup-deploy.png)

   >[!NOTE]
   >You can also edit, switch or add a program from Cloud Manager Overview page, as shown below:

   ![](/help/assets/create-program-a1.png)



## Creating a Regular Program {#create-regular-program}

A *Regular* program is intended for a user who is familiar with AEM and Cloud Manager and is ready to start writing, building and testing code with the objective of deploying it to Production.

Follow the steps below to create a regular program: 

1. Select **Set up for Production** in the Create Program wizard to create a regular program. User can accept the default program name or edit it before selecting **Continue**.

   ![](/help/assets/set-up-prod1.png)

1. User will select solutions that are to be included in the program in the screen that will be presented following the screen above.

   

   >[!NOTE]
   >
   >The screen below is only displayed for the segment of customers who have purchased more than one solution. For customers who have purchased just one solution, the solution selection screen below will not be displayed.

   ![](/help/assets/set-up-prod2.png)

1. Once you have selected the solutions, click **Create**.

   ![](/help/assets/set-up-prod3.png)

1.  Once you see your program card on the landing page, hover over it to select the Cloud Manager icon to navigate to the Cloud Manager **Overview** page. 

    ![](/help/assets/set-up-prod4.png)

1. The main call-to-action card will guide the user to create an environment, create a non-production pipeline, and finally a production pipeline.
   ![](/help/assets/set-up-prod5.png)


    >[!NOTE]
    >
    >A regular program does not have **Auto-setup** feature.

