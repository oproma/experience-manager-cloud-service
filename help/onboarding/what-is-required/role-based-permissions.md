---
title: "Role Based Permissions"
description: "Role Based Permissions"
sub-product: "Onboarding to AEM as a Cloud Service"
user-guide-title: "Onboarding to AEM as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/onboarding/what-is-required/role-based-permissions.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/onboarding/what-is-required/role-based-permissions.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/onboarding/what-is-required/role-based-permissions.md"
---
# Role Based Permissions {#role-based-permissions}

Cloud Manager has pre-configured roles with appropriate permissions. For example, a developer develops code and has the permission to push the code to the **Git Repository**. Alternatively, a business owner has different permissions allowing them to define the Key Performance Indicators (KPIs) and approve deployments.

## User Permissions {#user-permissions}

Each of the roles have specific permissions, pre-configured tasks, or permissions, associated with each role. This table lists the functions available and the roles who can execute the function.

|Permission|Description|Business Owner|Deployment Manager|Program Manager|Developer|
|--- |--- |--- |--- |--- |--- |
|Add Program|Add a New Program.|x||||
|Create Environment|Create Prod+Stage, Dev, Playground Environments.|x|x|||
|Update Environment|Update Prod+Stage, Dev, Playground Environments.|x|x|||
|Delete Environment|Delete Non-prod, Dev, Playground Environments.|x|x|||
|Delete Environment|Delete Prod+Stage Environment.|||||
|Program Setup|Configure Program (including KPIs).|x||||
|Program Setup|Git Commit Access.||x||x|
|Pipeline Setup|Setup or Edit Pipeline.||x|||
|Pipeline Execution|Start the Pipeline.|x|x|||
|Pipeline Execution|Reject/Approve Important 3-Tier Failures.|x|x|x||
|Pipeline Execution|Provide GoLive Approval.|x|x|x||
|Pipeline Execution|Schedule Production Deployment.|x|x|x||
|Pipeline Execution|Resume Production Pipeline.|||||
|Pipeline Delete|Allows Deleting of a Pipeline.||x|||
|Execution Cancel|Cancel Current Execution.||x|||
|Manage Environment|Add Publish-Dispatcher segment from the Manage Environment Screen.|x|x||||
|Push Update|Start Push Update Pipeline.|||||
|Generate Personal Access Token|Access Git.||x||x|

