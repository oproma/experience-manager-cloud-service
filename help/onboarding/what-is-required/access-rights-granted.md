---
title: "Access Rights Granted - What Is Required"
description: "Access Rights Granted - What Is Required"
sub-product: "Onboarding to AEM as a Cloud Service"
user-guide-title: "Onboarding to AEM as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/onboarding/what-is-required/access-rights-granted.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/onboarding/what-is-required/access-rights-granted.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/onboarding/what-is-required/access-rights-granted.md"
---
# Access Rights Granted {#access-rights-granted} 

Adobe will create an **Organization** identifier for your company in the Adobe Identity Management System (IMS), where all your users and their permissions can be managed. Each user, who needs to be a member of this organization, and will be granted access to any of the Experience Cloud service, will need to have their own **Adobe ID**. 

## User Identity Types {#user-identity-types}

To get started with an Adobe ID, please visit [Manage Adobe Identity Types](https://helpx.adobe.com/enterprise/using/identity.html) for detailed instructions on how to obtain an Adobe ID using one of the identity types available.

## Users and Roles {#users-and-roles}

Once Adobe has created an organization for your company, your designated administrator will be added as the first member to this organization. The administrator will be granted the administrator permissions by default, and assigned the AEM Managed Services **Product**, and one or more Cloud Manager **Product Profiles**. Please visit [Add Users and Roles](/help/onboarding/what-is-required/add-users-roles.md) to learn more about how to configure and manage your team users using the Admin Console.

With these rights granted, your administrator is now set up with a single sign-on (using Adobe ID) to access the Experience Cloud services, login to your AEM cloud environments, and use Cloud Manager.

