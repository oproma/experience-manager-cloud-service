---
title: "Applying Dynamic Media Image Presets"
description: "Learn how to apply image presets in Dynamic Media"
sub-product: "AEM Assets as a Cloud Service"
user-guide-title: "AEM Assets as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/assets/dynamic-media/image-presets.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/assets/dynamic-media/image-presets.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/assets/dynamic-media/image-presets.md"
---
# Applying Dynamic Media Image Presets {#applying-image-presets}

Image Presets enable assets to dynamically deliver images at different sizes, in different formats, or with other image properties there are generated dynamically. You can choose a preset when you export images, which also reformats images to the specifications that your administrator has specified.

In addition, you can choose an image preset that is responsive (designated by the **RESS** button after you select it).

This section describes how to use image presets. [Administrators can create and configure image presets](/help/assets/dynamicmedia/managing-image-presets.md).

>[!NOTE]
>
>Smart imaging works with your existing image presets and uses intelligence at the last millisecond of delivery to further reduce image file size based on browser or network connection speed. See [Smart Imaging](/help/assets/dynamicmedia/imaging-faq.md) for more information.

You can apply an image preset to an image anytime you preview it.

**To apply Dynamic Media Image Presets**

1. Open the asset and in the left rail, tap the drop-down menu, then tap **Renditions**.

   >[!NOTE]
   >
   >* Static renditions appear in the top half ofthepane. Dynamic renditions appear in the lower half. With dynamic renditions only, you can use the URL to display the image. The **URL** button only appears if you select a dynamic rendition. The **RESS** button only appears if you select a responsive image preset.
   >
   >* The system shows numerous renditions when you select **Renditions** in an asset's Detail view. You can increase the number of presets seen. See [Increasing the number of image presets that display](/help/assets/dynamicmedia/managing-image-presets.md#increasing-or-decreasing-the-number-of-image-presets-that-display).

   ![chlimage_1-208](/help/assets/chlimage_1-208.png)

1. Do any of the following:

    * Select a dynamic rendition to preview the image preset.
    * Tap **URL**, **Embed**, or **RESS** to display the pop-up.

   >[!NOTE]
   >
   >If the asset *and* the image preset are not yet published, the **URL** button (or **URL** and **RESS** buttons, if applicable) are not available.
   >
   >Note also that image presets are automatically published on a Dynamic Media S7 server.

