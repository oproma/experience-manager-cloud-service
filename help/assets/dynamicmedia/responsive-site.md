---
title: "Delivering Optimized Images for a Responsive Site"
description: "How to use the responsive code feature to deliver optimized images"
sub-product: "AEM Assets as a Cloud Service"
user-guide-title: "AEM Assets as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/assets/dynamic-media/responsive-site.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/assets/dynamic-media/responsive-site.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/assets/dynamic-media/responsive-site.md"
---
# Delivering optimized images for a responsive site {#delivering-optimized-images-for-a-responsive-site}

Use the Responsive code feature when you want to share the code for responsive serving with your web developer. You copy the responsive (**RESS**) code to the clipboard so you can share it with the web developer.

This feature makes sense to use if your website is on a third-party WCM. However, if your website is on AEM instead, an offsite image server renders the image and supplies it to the web page.

See also [Embedding the Video Viewer on a Web Page.](/help/assets/dynamicmedia/embed-code.md)

See also [Linking URLs to your Web Application.](/help/assets/dynamicmedia/linking-urls-to-yourwebapplication.md)

**To deliver optimized images for a responsive site**:

1. Navigate to the image you want supply responsive code for and in the drop-down menu, tap **Renditions**.

   ![chlimage_1-408](/help/assets/chlimage_1-408.png)

1. Select a responsive image preset. The **URL** and **RESS** buttons appear.

   ![chlimage_1-409](/help/assets/chlimage_1-409.png)

   >[!NOTE]
   >
   >The selected asset *and* the selected image preset or viewer preset must be published to make the **URL** or **RESS** buttons available.
   >
   >Image presets are automatically published.

1. Tap **RESS**.

    ![chlimage_1-410](/help/assets/chlimage_1-410.png)

1. In the **Embed Responsive Image** dialog box, select and copy the responsive code text and paste it in your web site to access the responsive asset.
1. Edit the default breakpoints in the embed code to match those of the responsive web site directly in the code. In addition, test the different image resolutions being served at different page breakpoints.

## Using HTTP/2 to delivery your Dynamic Media assets {#using-http-to-delivery-your-dynamic-media-assets}

HTTP/2 is the new, updated web protocol that improves the way browsers and servers communicate. It provides faster transfer of information and reduces the amount of processing power that is needed. Delivery of Dynamic Media assets is supported using HTTP/2 which provides better response and load times.

See [HTTP2 Delivery of Content](/help/assets/dynamicmedia/http2faq.md) for complete details on getting started using HTTP/2 with your Dynamic Media account.

