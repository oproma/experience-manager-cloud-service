---
title: "Setting Up Dynamic Media"
description: "To set up dynamic media, you need to configure dynamic media and manage image and viewer presets"
sub-product: "AEM Assets as a Cloud Service"
user-guide-title: "AEM Assets as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/assets/dynamic-media/administering-dynamic-media.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/assets/dynamic-media/administering-dynamic-media.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/assets/dynamic-media/administering-dynamic-media.md"
---
# Setting Up Dynamic Media {#setting-up-dynamic-media}

[Dynamic Media](https://www.adobe.com/solutions/web-experience-management/dynamic-media.html) helps you manage assets by delivering rich visual merchandising and marketing assets on demand, automatically scaled for consumption on web, mobile, and social sites. Using a set of master assets, Dynamic Media generates and delivers multiple variations of rich content in real time through its global, scalable, performance-optimized network.

<!-- OBSOLETE UNTIL THE INTEGRATING SCENE7 TOPIC GETS A MAJOR UPDATE

>[!NOTE]
>
>This documentation describes Dynamic Media capabilites, which are integrated directly into AEM. If you are using Dynamic Media Classic (previously called Scene7) integrated into AEM, see [Dynamic Media Classic integration documentation](/help/sites-cloud/administering/integrating-scene7.md).
>
>See [Dual Use Scenario](/help/sites-cloud/administering/integrating-scene7.md#dual-use-scenario) for times when you may want to use AEM integrated with Dynamic Media Classic along with Dynamic Media.

-->

If you are administering Dynamic Media, the following topics are of interest:

* [Configuring Dynamic Media](/help/assets/dynamicmedia/config-dm.md)
* [Managing Image Presets](/help/assets/dynamicmedia/managing-image-presets.md)
* [Managing Viewer Presets](/help/assets/dynamicmedia/managing-viewer-presets.md)
* [Troubleshooting Dynamic Media](/help/assets/dynamicmedia/troubleshoot-dm.md)

See also the following topics:

* [Video encoding and video profiles](/help/assets/dynamicmedia/video-profiles.md)
* [Image profiles](/help/assets/dynamicmedia/image-profiles.md)

>[!NOTE]
>
>**If you are upgrading:**
>
>* After you have AEM up and running, any asset you upload has Dynamic Media automatically enabled (unless it was explicitly disabled by your system administrator). If you are on an upgraded instance of AEM and new to Dynamic Media, you may need to re-process your assets to make them Dynamic Media-enabled.

