---
title: "Publishing Dynamic Media Assets"
description: "How to publish dynamic media assets"
sub-product: "AEM Assets as a Cloud Service"
user-guide-title: "AEM Assets as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/assets/dynamic-media/publishing-dynamicmedia-assets.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/assets/dynamic-media/publishing-dynamicmedia-assets.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/assets/dynamic-media/publishing-dynamicmedia-assets.md"
---
# Publishing Dynamic Media Assets {#publishing-dynamic-media-assets}

You publish your Dynamic Media assets by selecting the assets and tapping **Publish**. After your dynamic media assets are published, they are available to you for including in a web page via URL or by way of embedding.

You can also instantly publish assets that you upload&mdash;without any user intervention. Or, you can selectively publish those assets. See [Configuring Dynamic Media](/help/assets/dynamicmedia/config-dm.md).

In the **Card View**, a small globe icon appears directly below an asset's name to indicate that it is published. In the **List View**, a **Published** column indicates which assets are published or which are not.

>[!NOTE]
>
>If an asset is already published, then you use AEM to move the asset to another folder, and re-publish from its new location, the original published asset location is still available, along with the newly re-published asset. The original published asset, however, is "lost" to AEM and cannot be unpublished. Therefore, as a best practice, unpublish assets first before you move them to a different folder.

If you intend to publish video assets immediately after encoding them, make sure encoding is completely done. When videos are still being encoded, the system lets you know that a video processing workflow is in progress. When video encoding is done, you should be able to preview the video renditions. At that point, it is safe for you to publish the videos without incurring any publishing errors.

See also [Linking URLs to your Web Application](/help/assets/dynamicmedia/linking-urls-to-yourwebapplication.md).

See also [Embedding the Video Viewer on a Web Page.](/help/assets/dynamicmedia/embed-code.md)

>[!NOTE]
>
>* Assets must be published in order to use the URL. If the assets are not published, copying and pasting the URL into a web browser will not work.
>* Image presets and viewer presets must be activated and published for live delivery.
>

For detailed information on publishing a set or asset, see [Publishing Assets.](/help/assets/manage/manage-digital-assets.md)

## HTTP/2 delivery of Dynamic Media assets {#http-delivery-of-dynamic-media-assets}

AEM now supports the delivery of all Dynamic Media content (images and video) over HTTP/2. That is, a published URL or embed code for the image or video is available to be integrated with any application that accepts a hosted asset. That published asset is then delivered by way of HTTP/2 protocol. This method of delivery improves the way browsers and servers communicate, allowing for better response and load times of all your Dynamic Media assets.

See [HTTP/2 delivery of content frequently asked questions](/help/assets/dynamicmedia/http2faq.md) to learn more. 
<!--this md file used to reside under sites-administering-->

