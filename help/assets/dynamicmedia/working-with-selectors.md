---
title: "Working with Selectors"
description: "Selecting assets for interactive images, interactive video, and carousel banners"
sub-product: "AEM Assets as a Cloud Service"
user-guide-title: "AEM Assets as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/assets/dynamic-media/working-with-selectors.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/assets/dynamic-media/working-with-selectors.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/assets/dynamic-media/working-with-selectors.md"
---
# Working with Selectors in Dynamic Media {#working-with-selectors}

When working with an Interactive Image, Interactive Video, or Carousel Banner, you select assets and you select sites and products for hotspots and image maps to link to. When working with Image Sets, Spin Sets, and Multimedia Sets, you also select assets with the Asset Selector.

This topic covers how to use the Product, Site, and Asset selectors, including the ability to browse, filter, sort within the selectors.

You access the selectors while creating carousel sets, adding hotspots and image maps, creating interactive videos and images.

For example, in this Carousel Banner, you use the Product selector if you are linking a hotspot or image map to a Quickview page; use the Site selector if you are linking a hotspot or image map to a Hyperlink; use the Asset selector when you are creating a new slide.

![chlimage_1-520](/help/assets/chlimage_1-520.png)

When you select (rather than manually enter) where hotspots or image maps go to, you are using the selector. The Site selector only works if you are an AEM Sites customer. The product selector also requires AEM Commerce.

## Selecting products in Dynamic Media {#selecting-products}

Use the Product selector to choose a product when you want a hotspot or image map to provide a Quickview to a specific product in your product catalog.

1. Navigate to the Carousel Set, Interactive Image, or Interactive Video, and tap the **Actions** tab (only available if you have defined a hotspot or image map).

   The Product selector is in the **Action Type** area.

   ![chlimage_1-521](/help/assets/chlimage_1-521.png)

1. Tap the **Product Selector** icon (magnifying glass) and navigate to a product in the catalog.

   ![chlimage_1-522](/help/assets/chlimage_1-522.png)

   You can also filter by keyword or tag by tapping **Filter** and entering keywords, or selecting tags, or both.

   ![chlimage_1-523](/help/assets/chlimage_1-523.png)

   You can change where AEM browses for product data by tapping **Browse** and navigating to another folder.

   ![chlimage_1-524](/help/assets/chlimage_1-524.png)

   Tap **Sort** by to change whether AEM sorts by newest to oldest or oldest to newest.

   ![chlimage_1-525](/help/assets/chlimage_1-525.png)

   Tap **View as** to change how you view products &ndash; either **List View** or **Card View**.

   ![chlimage_1-526](/help/assets/chlimage_1-526.png)

1. After the product is selected, the field populates with the product thumbnail and name.

   ![chlimage_1-527](/help/assets/chlimage_1-527.png)

1. When in **Preview** mode, you can tap the hotspot or image map, and see what the Quickview looks like.

   ![chlimage_1-528](/help/assets/chlimage_1-528.png)

## Selecting Sites in Dynamic Media {#selecting-sites}

Use the site selector to choose a webpage when you want a hotspot or image map to link to a webpage that is managed within AEM sites.

1. Navigate to the Carousel Set, Interactive Image, or Interactive Video, and tap the **Actions** tab (only available if you have defined a hotspot or image map).

   The Site Selector is in the **Action Type** area.

   ![chlimage_1-529](/help/assets/chlimage_1-529.png)

1. Tap the **Site Selector** icon (folder with magnifying glass) and navigate to a page in your AEM sites that you want to link the hotspot or image map to.

   ![chlimage_1-530](/help/assets/chlimage_1-530.png)

1. After the site is selected, the field populates with the path.

   ![chlimage_1-531](/help/assets/chlimage_1-531.png)

1. When in **Preview** mode if you tap the hotspot or image map, you navigate to the AEM site page you specified.

## Selecting assets in Dynamic Media {#selecting-assets}

Use this selector to choose images for use in a Carousel Banner, an Interactive Video, Image Sets, Mixed Media Sets, and Spin Sets. In interactive Video, the asset selector is available when you tap **Select Assets** in the **Content** tab. In Carousel Sets, the asset selector is available when you create a new slide. In Image Sets, Mixed Media Sets, and Spin Sets, the asset selector is available when you create a new Image Set, Mixed Media Set, or Spin Set, respectively.

See also [Asset Picker](/help/assets/manage/search-assets.md#assetselector) for more information.

1. Navigate to the Carousel Set and create a new slide. Or, navigate to the Interactive Video, go the **Content** tab and select assets. Or, create a Mixed Media Set, Image Set, or Spin Set.
1. Tap the **Asset Selector** icon (folder with magnifying glass) and navigate to an asset.

   ![chlimage_1-532](/help/assets/chlimage_1-532.png)

   You can also filter by keyword or tag by tapping **Filter** and entering keywords, or adding criteria, or both.

   ![chlimage_1-533](/help/assets/chlimage_1-533.png)

   You can change where AEM browses for assets by navigating to another folder in the **Path** field.

   Tap **Collection** to only search for assets within collections.

   ![chlimage_1-534](/help/assets/chlimage_1-534.png)

   Tap **View as** to change how you view products - either **List View**, **Column View**, or **Card View**.

   ![chlimage_1-535](/help/assets/chlimage_1-535.png)

1. Tap the check mark to select the asset. The asset is displayed.

   ![chlimage_1-536(/help/assets/chlimage_1-536.png)
-->

