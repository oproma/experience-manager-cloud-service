---
title: "About managing Dynamic Media assets"
description: "Learn how to work with Dynamic Media assets"
sub-product: "AEM Assets as a Cloud Service"
user-guide-title: "AEM Assets as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/assets/dynamic-media/managing-assets.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/assets/dynamic-media/managing-assets.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/assets/dynamic-media/managing-assets.md"
---
# About managing Dynamic Media assets {#managing-assets}

Managing Dynamic Media assets&ndash;images, video, and interactive assets&ndash;after they are uploaded&ndash;involves many tasks including previewing, downloading, or publishing them.

When previewing, you can also apply image presets (for images only) or viewer presets (for video and images). You cannot apply both to an asset. See [Applying viewer presets](/help/assets/dynamicmedia/viewer-presets.md) and [Applying image presets](/help/assets/dynamicmedia/image-presets.md).

General information on managing assets is found in [Managing Assets with the Touch UI](/help/assets/manage/manage-digital-assets.md).

The following topics describe what you need to do to manage Dynamic Media assets:

* [Best practices for optimizing the quality of your images](/help/assets/dynamicmedia/best-practices-for-optimizing-the-quality-of-your-images.md)
* [Previewing Dynamic Media assets](/help/assets/dynamicmedia/previewing-assets.md)
* [Publishing Dynamic Media Assets](/help/assets/dynamicmedia/publishing-dynamicmedia-assets.md)
* [Working with Selectors](/help/assets/dynamicmedia/working-with-selectors.md)

