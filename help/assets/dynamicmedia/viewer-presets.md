---
title: "Applying Dynamic Media viewer presets"
description: "Learn how to apply viewer presets in Dynamic Media"
sub-product: "AEM Assets as a Cloud Service"
user-guide-title: "AEM Assets as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/assets/dynamic-media/viewer-presets.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/assets/dynamic-media/viewer-presets.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/assets/dynamic-media/viewer-presets.md"
---
# Applying Dynamic Media viewer presets {#applying-viewer-presets}

A Viewer Preset is a collection of settings that determine how users view rich-media assets on their computer screens and mobile devices. You can apply any viewer presets created by your administrator to an asset.

If you are an administrator and need to manage, create, sort, and delete viewer presets, see [Managing Viewer Presets](/help/assets/dynamicmedia/managing-viewer-presets.md).

See also [Publishing Viewer Presets](/help/assets/dynamicmedia/managing-viewer-presets.md#publishing-viewer-presets).

You may not need to publish viewer presets depending on what publish mode you are using.
Any problems with viewer presets, see [Troubleshooting Dynamic Media - Scene7](/help/assets/dynamicmedia/troubleshoot-dm.md#viewers).

## Applying a Dynamic Media viewer preset to an asset {#applying-a-viewer-preset-to-an-asset}

1. Open the asset and in the left rail, and tap **Viewers**.

   ![chlimage_1-104](/help/assets/chlimage_1-104.png)

   * The **URL** and **Embed** buttons appear after you select a viewer preset.
   * The system shows numerous viewer presets when you select Viewers in an asset's **Detail View**. You can increase the number of presets seen. See [Increasing the number of viewer presets that display](/help/assets/dynamicmedia/managing-viewer-presets.md).

1. Select a viewer from the left pane to apply it to the asset as seen in the right pane. You can also [copy the URL to share](/help/assets/dynamicmedia/linking-urls-to-yourwebapplication.md) with others users.

## Obtaining viewer preset URLs {#obtaining-viewer-preset-urls}

To get the URLs for Viewer Presets, see [Linking URLs to your Web application](/help/assets/dynamicmedia/linking-urls-to-yourwebapplication.md).

