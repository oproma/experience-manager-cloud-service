---
title: "Delivering Dynamic Media Assets"
description: "Learn how to deliver dynamic media assets"
sub-product: "AEM Assets as a Cloud Service"
user-guide-title: "AEM Assets as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/assets/dynamic-media/delivering-dynamic-media-assets.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/assets/dynamic-media/delivering-dynamic-media-assets.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/assets/dynamic-media/delivering-dynamic-media-assets.md"
---
# Delivering Dynamic Media Assets{#delivering-dynamic-media-assets}

How you can deliver your dynamic media assets - both video and images - depends on how your website is implemented.

With Dynamic Media you have several options:

* If your website is hosted on AEM, then you want to add the dynamic media assets directly to your page.
* If your website is not on AEM, then you have the choice of either:

  * Embedding your video or image on your website.
  * Link URLs to your web application. Use linking when you want to deliver a video player as a pop-up or modal window.
  * If your site is responsive, you can [deliver optimized images.](/help/assets/dynamicmedia/responsive-site.md)

>[!NOTE]
>
>Smart imaging works with your existing image presets and uses intelligence at the last millisecond of delivery to further reduce image file size based on browser or network connection speed. See [Smart Imaging](/help/assets/dynamicmedia/imaging-faq.md) for more information.

For more information, see the following topics:

* [Adding Dynamic Media Assets to Web Pages](/help/assets/dynamicmedia/adding-dynamic-media-assets-to-pages.md)
* [Embedding the Video or Image Viewer on a Web Page](/help/assets/dynamicmedia/embed-code.md)
* [Activating hotlink protection in Dynamic Media](/help/assets/dynamicmedia/hotlink-protection.md)
* [Linking URLs to your Web Application](/help/assets/dynamicmedia/linking-urls-to-yourwebapplication.md)
* [Delivering Optimized Images for a Responsive Site](/help/assets/dynamicmedia/responsive-site.md)
* [HTTP2 Delivery of Content](/help/assets/dynamicmedia/http2faq.md)
* [Invalidating your CDN cached content](/help/assets/dynamicmedia/invalidate-cdn-cached-content.md)
* [Using Rulesets to Transform URLs](/help/assets/dynamicmedia/using-rulesets-to-transform-urls.md)

## HTTP/2 delivery of Dynamic Media assets {#http-delivery-of-dynamic-media-assets}

AEM now supports the delivery of all Dynamic Media content (images and video) over HTTP/2. That is, a published URL or embed code for the image or video is available to be integrated with any application that accepts a hosted asset. That published asset is then delivered by way of HTTP/2 protocol. This method of delivery improves the way browsers and servers communicate, allowing for better response and load times of all your Dynamic Media assets.

See [HTTP/2 Delivery of Content Frequently Asked Questions](/help/assets/dynamicmedia/http2faq.md) to learn more.

