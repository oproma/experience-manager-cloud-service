---
title: "Working with Dynamic Media"
description: "Learn how to use Dynamic Media to deliver assets for consumption on web, mobile, and social sites."
sub-product: "AEM Assets as a Cloud Service"
user-guide-title: "AEM Assets as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/assets/dynamic-media/dynamic-media.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/assets/dynamic-media/dynamic-media.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/assets/dynamic-media/dynamic-media.md"
---
# Working with Dynamic Media {#working-with-dynamic-media}

[Dynamic Media](https://www.adobe.com/solutions/web-experience-management/dynamic-media.html) helps deliver rich visual merchandising and marketing assets on demand, automatically scaled for consumption on web, mobile, and social sites. Using a set of master assets, Dynamic Media generates and delivers multiple variations of rich content in real time through its global, scalable, performance-optimized network.

Dynamic media serves interactive viewing experiences, including zoom, 360-degree spin, and video. Dynamic media uniquely incorporates the workflows of the Adobe Experience Manager digital asset management (Assets) solution to simplify and streamline the digital campaign management process.

>[!NOTE]
>
>A Community article is available on [Working with Adobe Experience Manager and Dynamic Media](https://helpx.adobe.com/experience-manager/using/aem_dynamic_media.html).

## What you can do with Dynamic Media {#what-you-can-do-with-dynamic-media}

Dynamic Media lets you manage your assets before publishing them. How to work with assets in general is covered in detail in [Working with Digital Assets](/help/assets/manage/manage-digital-assets.md). General topics include uploading, downloading, editing, and publishing assets; viewing and editing properties, and searching for assets.

Dynamic Media-only features include the following:

* [Carousel Banners](/help/assets/dynamicmedia/carousel-banners.md)
* [Image Sets](/help/assets/dynamicmedia/image-sets.md)
* [Interactive Images](/help/assets/dynamicmedia/interactive-images.md)
* [Interactive Videos](/help/assets/dynamicmedia/interactive-videos.md)
* [Mixed Media Sets](/help/assets/dynamicmedia/mixed-media-sets.md)
* [Panoramic Images](/help/assets/dynamicmedia/panoramic-images.md)

* [Spin Sets](/help/assets/dynamicmedia/spin-sets.md)
* [Video](/help/assets/dynamicmedia/video.md)
* [Delivering Dynamic Media Assets](/help/assets/dynamicmedia/delivering-dynamic-media-assets.md)
* [Managing Assets](/help/assets/dynamicmedia/managing-assets.md)
* [Using Quickviews to create custom pop-ups](/help/assets/dynamicmedia/custom-pop-ups.md)

See also [Setting up Dynamic Media](/help/assets/dynamicmedia/administering-dynamic-media.md).

<!-- 

OBSOLETE UNTIL INTEGRATING SCENE7 TOPIC GETS A MAJOR UPDATE
>[!NOTE]
>
>To understand the differences between using Dynamic Media and integrating Dynamic Media Classic with AEM, see [Dynamic Media Classic integration versus Dynamic Media](/help/sites-cloud/administering/integrating-scene7.md#aem-scene-integration-versus-dynamic-media).

-->

## Dynamic Media enabled versus Dynamic Media disabled {#dynamic-media-on-versus-dynamic-media-off}

You can tell whether Dynamic Media is enabled (turned on) by the following characteristics:

* Dynamic renditions are available when downloading or previewing assets.
* Image sets, spin sets, mixed media sets are available.
* PTIFF renditions are created.

When you click an image asset, the view of the asset is different with Dynamic Media enabled. Dynamic Media uses the on-demand HTML5 viewers.

### Dynamic renditions {#dynamic-renditions}

Dynamic renditions such as image and viewer presets (under **Dynamic**) are available when Dynamic Media is enabled.

![chlimage_1-358](/help/assets/chlimage_1-358.png)

### Image sets, spins sets, mixed media sets {#image-sets-spins-sets-mixed-media-sets}

Image sets, spin sets, and mixed media sets are available if Dynamic Media is enabled.

![chlimage_1-359](/help/assets/chlimage_1-359.png)

### PTIFF renditions {#ptiff-renditions}

Dynamic media enabled assets include `pyramid.tiffs`.

![chlimage_1-360](/help/assets/chlimage_1-360.png)

### Asset views change {#asset-views-change}

With Dynamic Media enabled, you can zoom in and out by clicking the `+` and `-` buttons. You can also click/tap to zoom into certain area. Revert brings you to the original version and you can make the image full screen by clicking the diagonal arrows. Dynamic Media enabled looks like this:

![chlimage_1-361](/help/assets/chlimage_1-361.png)

With Dynamic Media disabled you can zoom in and out and revert to the original size:

![chlimage_1-362](/help/assets/chlimage_1-362.png)

