---
title: "Embedding the Dynamic Media Video or Image viewer on a web page"
description: "Learn how to embed Dynamic Media video or images on a web page"
sub-product: "AEM Assets as a Cloud Service"
user-guide-title: "AEM Assets as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/assets/dynamic-media/embed-code.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/assets/dynamic-media/embed-code.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/assets/dynamic-media/embed-code.md"
---
# Embedding the Dynamic Media Video or Image viewer on a web page {#embedding-the-video-or-image-viewer-on-a-web-page}

Use the **Embed Code** feature when you want to play the video or view an asset embedded on a web page. You copy the embed code to the clipboard so you can paste it in your web pages. Editing of the code is not permitted in the **Embed Code** dialog box.

You embed URLs only if you are _not_ using AEM as your WCM. If you are using AEM as your WCM, [you add the assets directly on your page.](/help/assets/dynamicmedia/adding-dynamic-media-assets-to-pages.md)

See [Linking URLs to your Web Application.](/help/assets/dynamicmedia/linking-urls-to-yourwebapplication.md)

See [Delivering Optimized Images for a Responsive Site.](/help/assets/dynamicmedia/responsive-site.md)

>[!NOTE]
>
>The embed code is not available to copy until you have published the selected asset. In addition, you must also publish the viewer preset or image preset.
>
>See [Publishing Assets](/help/assets/dynamicmedia/publishing-dynamicmedia-assets.md).
>
>See [Publishing Viewer Presets](/help/assets/dynamicmedia/managing-viewer-presets.md#publishing-viewer-presets).
>
>See [Publishing Image Presets](/help/assets/dynamicmedia/managing-image-presets.md#publishing-image-presets).

**To embed the Dynamic Media Video or Image viewer on a web page**

1. Navigate to the *published* video or image asset whose embed code you want to copy.

   Remember that the embed code is only available to copy *after* you have first *published* the assets. In addition, the viewer preset or image preset must also be published.

   See [Publishing Assets.](/help/assets/dynamicmedia/publishing-dynamicmedia-assets.md)

   See [Publishing Viewer Presets](/help/assets/dynamicmedia/managing-viewer-presets.md#publishing-viewer-presets).

   See [Publishing Image Presets](/help/assets/dynamicmedia/managing-image-presets.md#publishing-image-presets).

1. In the left rail, select the drop-down menu and tap **Viewers**.
1. In the left rail, tap a viewer preset name. The viewer preset is applied to the asset.
1. Tap **Embed**.
1. In the **Embed Code** dialog box, copy the entire code to the clipboard, and then tap **Close**.
1. Paste the embed code into your web pages.

## Using HTTP/2 to deliver your Dynamic Media assets {#using-http-to-deliver-your-dynamic-media-assets}

HTTP/2 is the new, updated web protocol that improves the way browsers and servers communicate. It provides faster transfer of information and reduces the amount of processing power that is needed. Delivery of Dynamic Media assets can now be over HTTP/2 which provides better response and load times.

See [HTTP2 Delivery of Content](/help/assets/dynamicmedia/http2faq.md) for complete details on getting started using HTTP/2 with your Dynamic Media account.

