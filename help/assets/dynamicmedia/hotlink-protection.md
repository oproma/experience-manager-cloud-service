---
title: "Activating hotlink protection in Dynamic Media"
description: "Information on how to activate hotlink protection in Dynamic Media."
sub-product: "AEM Assets as a Cloud Service"
user-guide-title: "AEM Assets as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/assets/dynamic-media/hotlink-protection.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/assets/dynamic-media/hotlink-protection.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/assets/dynamic-media/hotlink-protection.md"
---
# Activating hotlink protection in Dynamic Media {#activating-hotlink-protection-in-dynamic-media}

Hotlinking is when a third-party website uses HTML code to display an image from your website. They use your bandwidth every time the picture is requested because the visitor's browser is accessing it directly from your server. Hotlink *protection* is a method to prevent other websites from directly linking to pictures, css, or javascript on your webpages. This kind of shield helps reduce unnecessary bandwidth usage under your Dynamic Media account.

[Adobe Support](https://helpx.adobe.com/support.html) can configure a referrer filter at the CDN level so that Dynamic Media content is only served to websites on your list of permitted websites for the domain.

Hotlink protection requires that you use Adobe’s bundled CDN. To activate hotlink protection, an Administrator must create a support ticket to request the configuration change to your Dynamic Media account. There is no additional cost for activating hotlink protection.

