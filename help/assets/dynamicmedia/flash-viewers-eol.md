---
title: "Flash Viewers End-of-Life Notice"
description: "Effective January 31, 2017, Adobe Scene7 will officially end-of-life support for the Flash viewer platform."
sub-product: "AEM Assets as a Cloud Service"
user-guide-title: "AEM Assets as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/assets/dynamic-media/flash-viewers-eol.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/assets/dynamic-media/flash-viewers-eol.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/assets/dynamic-media/flash-viewers-eol.md"
---
# Flash Viewers End-of-Life Notice{#flash-viewers-end-of-life-notice}

*Effective January 31, 2017, Adobe Dynamic Media Classic (Scene7) officially ended support for the Flash viewer platform.*

*For more information about this important change, see the following FAQ website:*

[https://docs.adobe.com/content/docs/en/aem/6-1/administer/integration/marketing-cloud/scene7/flash-eol.html](https://experienceleague.adobe.com/docs/en/aem/6-1/administer/integration/marketing-cloud/scene7/flash-eol.html).

