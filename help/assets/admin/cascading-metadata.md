---
title: "Cascading metadata"
description: "This article describes how to define cascading metadata for assets."
contentOwner: "AG"
sub-product: "AEM Assets as a Cloud Service"
user-guide-title: "AEM Assets as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/assets/cascading-metadata.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/assets/cascading-metadata.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/assets/cascading-metadata.md"
---
# Cascading Metadata {#cascading-metadata}

When capturing the metadata information of an asset, users provide information in the various available fields. You can display specific metadata fields or field values that are dependent on the options selected in the other fields. Such conditional display of metadata is called cascading metadata. In other words, you can create a dependency between a particular metadata field/value and one or more fields and/or their values.

Use metadata schemas to define rules for displaying cascading metadata. For example, if your metadata schema includes an asset type field, you can define a pertinent set of fields to be displayed based on the type of asset a user selects.

Here are some use cases for which you can define cascading metadata:

* Where user location is required, display relevant city names based on the user's choice of country and state.
* Load pertinent brand names in a list based on the user's choice of product category.
* Toggle the visibility of a particular field based on the value specified in another field. For example, display separate shipping address fields if the user wants the shipment delivered at a different address.
* Designate a field as mandatory based on the value specified in another field.
* Change options displayed for a particular field based on the value specified in another field.
* Set the default metadata value in a particular field based on the value specified in another field.

## Configure cascading metadata in AEM {#configure-cascading-metadata-in-aem}

Consider a scenario where you want to display cascading metadata based on the type of asset that is selected. Some examples

* For a video, display applicable fields such as format, codec, duration, and so on.
* For a Word or PDF document, display fields, such as page count, author, and so on.

Irrespective of the asset type chosen, display the copyright information as a required field.

1. Tap/click the AEM logo, and go to **Tools** > **Assets** > **Metadata Schemas**.
1. In the **Schema Forms** page, select a schema form and then tap/click **Edit** from the toolbar to edit the schema.

   ![select_form](/help/assets/select_form.png)

1. (Optional) In the metadata schema editor, create a new field to conditionalize. Specify a name and property path in the **Settings** tab.

   To create a new tab, tap/click `+` to add a tab and then add a metadata field.

   ![add_tab](/help/assets/add_tab.png)

1. Add a Dropdown field for asset type. Specify a name and property path in the **Settings** tab. Add an optional description.

   ![asset_type_field](/help/assets/asset_type_field.png)

1. Key-values pairs are the options provided to a form-user. You can provide the key-value pairs either manually or from a JSON file.

    * To specify the values manually, select **Add Manually**, and tap/click **Add Choice** and specify the option text and value. For example, specify Video, PDF, Word, and Image asset types.

    * To fetch the values from a JSON file dynamically, select **Add Through JSON Path** and provide the path of the JSON file. AEM fetches the key-value pairs in the real time when the form is presented to the user.

   Both options are mutually exclusive. You cannot import the options from a JSON file and edit manually.

   ![add_choice](/help/assets/add_choice.png)

   >[!NOTE]
   >
   >When you add a JSON file, the key-value pairs are not displayed in the metadata schema editor but are available in the published form.

   >[!NOTE]
   >
   >When adding choices, if you click the pop-up field, the interface is distorted and the delete icon for the choices stops working. Do not click on the dropdown till you save the changes. If you face this issue, save the schema and open it again to continue editing.

1. (Optional) Add the other required fields. For example, format, codec, and duration for the asset type video.

   Similarly, add dependent fields for other asset types. For example, add fields page count and author for document assets, such as PDF and Word files.

   ![video_dependent_fields](/help/assets/video_dependent_fields.png)

1. To create a dependency between the asset type field and other fields, choose the dependent field and open the **Rules** tab.

   ![select_dependentfield](/help/assets/select_dependentfield.png)

1. Under **Requirement**, choose the **Required, based on new rule** option.
1. Tap/click **Add Rule** and choose the **Asset Type** field to create a dependency. Also choose the field value upon which to create the dependency. In this case, choose **Video**. Tap/click **Done** to save the changes.

   ![define_rule](/help/assets/define_rule.png)

   >[!NOTE]
   >
   >Dropdown with manually predefined values can be used with rules. Dropdown menus with configured JSON path can't be used with rules that use predefined values to apply conditions. If the values are loaded from JSON at runtime, it is not possible to apply a predefined rule.

1. Under **Visibility**, choose the **Visible, based on new rule** option.

1. Tap/click **Add Rule** and choose the **Asset Type** field to create a dependency. Also choose the field value upon which to create the dependency. In this case, choose **Video**. Tap/click **Done** to save the changes.

   ![define_visibilityrule](/help/assets/define_visibilityrule.png)

   >[!CAUTION]
   >
   >To reset the values, click or tap on whitespace or anywhere on the interface other than the values. If the values are reset, select the values again.

   >[!NOTE]
   >
   >You can apply **Requirement** condition and **Visibility** condition independent of each other.

1. Similarly, create a dependency between the value Video in the Asset Type field and other fields, such as Codec and Duration.
1. Repeat the steps to create dependency between document assets (PDF and Word) in the Asset Type field and fields such as Page Count and Author.
1. Click **Save**. Apply the metadata schema to a folder.

1. Navigate to the folder to which you applied the Metadata Schema and open the properties page of an asset. Depending upon your choice in the Asset Type field, pertinent cascading metadata fields are displayed.

   ![Cascading metadata for Video asset](/help/assets/video_asset.png)
   *Figure: Cascading metadata for Video asset*

   ![Cascading metadata for document asset](/help/assets/doc_type_fields.png)
   *Figure: Cascading metadata for document asset*

