---
title: "Associated Content"
description: "Associated content provides the connection so that assets can be (optionally) used with the fragment when it is added to a content page."
sub-product: "AEM Assets as a Cloud Service"
user-guide-title: "AEM Assets as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/assets/content-fragments/content-fragments-assoc-content.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/assets/content-fragments/content-fragments-assoc-content.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/assets/content-fragments/content-fragments-assoc-content.md"
---
# Associated Content{#associated-content}

Associated content provides the connection so that assets (i.e.) can be (optionally) used with the fragment when it is added to a content page. This provides flexibility by [providing a range of assets to access when using the content fragment on a page](/help/sites/authoring/fundamentals/content-fragments.md#using-associated-content), while also helping to reduce the time required to search for the appropriate asset.

## Adding Associated Content {#adding-associated-content}

>[!NOTE]
>
>There are various methods of adding [visual assets (e.g. images)](/help/assets/content-fragments/content-fragments.md#fragments-with-visual-assets) to the fragment and/or page.

To make the association you first need to [add your media asset(s) to a collection](/help/assets/manage/manage-collections.md). After that is done you can:

1. Open your fragment and select **Associated Content** from the side panel.

   ![Associated Content](/help/assets/cfm-assoc-content-01.png)

2. Select **Associate Content** or **Associate Collection** (as appropriate, depending on whether any collections have already been associated or not).
3. Select the required collection.

   You can optionally add the fragment itself to the selected collection; this aids tracking.

   ![Select collection](/help/assets/cfm-assoc-content-02.png)

4. Confirm (with the check mark). The collection will be listed as associated.

   ![cfm-6420-05](/help/assets/cfm-assoc-content-03.png)

## Editing Associated Content {#editing-associated-content}

Once you have associated a collection you can:

* **Remove** the association.
* **Add Assets** to the collection.
* Select an asset for further action.
* Edit the asset.

