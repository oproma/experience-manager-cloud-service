---
title: "Metadata - Fragment Properties"
description: "You can view and edit the metadata (properties) for content fragments."
sub-product: "AEM Assets as a Cloud Service"
user-guide-title: "AEM Assets as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/assets/content-fragments/content-fragments-metadata.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/assets/content-fragments/content-fragments-metadata.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/assets/content-fragments/content-fragments-metadata.md"
---
# Metadata - Fragment Properties{#metadata-fragment-properties}

## Editing Properties / Meta data {#editing-properties-meta-data}

You can view and edit the metadata (properties) for content fragments:

1. In the **Assets** console navigate to the location of the content fragment.
2. Either:

    * Select [**View Properties** to open the dialogs](/help/assets/manage/manage-digital-assets.md#editing-properties). Once open for viewing you can also edit.
    * Open the [content fragment for Edit](/help/assets/content-fragments/content-fragments-managing.md#opening-the-fragment-editor), then select **Metadata** from the side panel.

   ![metadata](/help/assets/cfm-metadata-01.png)

3. The **Basic** tab provides options that you can view or edit:

    * Thumbnail, for which you can **Upload Image**
    * **Title**
    * **Description**
    * **Tags**
    * Created (display only)

   ![metadata](/help/assets/cfm-metadata-02.png)

