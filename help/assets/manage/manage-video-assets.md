---
title: "Manage video assets"
description: "Learn how to upload, preview, annotate, and publish video assets."
contentOwner: "AG"
sub-product: "AEM Assets as a Cloud Service"
user-guide-title: "AEM Assets as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/assets/manage-video-assets.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/assets/manage-video-assets.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/assets/manage-video-assets.md"
---
# Manage video assets {#manage-video-assets}

Learn how to manage and edit the video assets in Adobe Experience Manager (AEM) Assets. Video encoding and transcoding, for example, FFmpeg transcoding, is possible only with Dynamic Media integration. Without Dynamic Media, you get basic support for videos, such as extraction of preview thumbnails for the supported file formats and preview in the user interface for formats that are supported for playback in the browser directly.

<!-- Also, if you are licensed to use Dynamic Media, see the [Dynamic Media video documentation](/help/assets/dynamic-media/video.md). -->

## Upload and preview video assets {#upload-and-preview-video-assets}

Adobe Experience Manager Assets generates previews for video assets with the extension MP4. You can preview these renditions in the AEM Assets user interface.

1. In the Digital Assets folder or subfolders, navigate to the location where you want to add digital assets.
1. To upload the asset, click or tap **Create** from the toolbar and choose **Files**. Alternatively, drag a file on the user interface. See [upload assets](/help/assets/manage/manage-digital-assets.md#uploading-assets) for details.
1. To preview a video in the Card view, tap the **Play** button on the video asset. You can pause or play video in the card view only. The Play and Pause buttons are not available in the list view.
1. To preview the video in the asset details page, click or tap the **Edit** icon on the card. The video plays in the native video player of the browser. You can play, pause, control the volume, and zoom the video to full screen.

## Publish video assets {#publish-video-assets}

After your video assets are published, they are available to you for including in a web page by way of a URL or embedding on a web page. See [publishing assets](/help/assets/dynamicmedia/publishing-dynamicmedia-assets.md).

## Annotate video assets {#annotate-video-assets}

1. From the Assets console, click or tap the Edit icon on the asset card to display the asset details page.
1. To play the video, click or tap the Preview icon.
1. To annotate the video, click the **Annotate** button. An annotation is added at the particular time-point (frame) in the video. When annotating, you can draw on the canvas and include a comment with the drawing. Comments are auto-saved. To exit the annotation wizard, click **Close**.
1. Seek to a specific point in the video, specify the time in seconds in the **text** field and click **Jump**. For example, to skip the first 10 seconds of video, enter 20 in the text field.
1. To view it in the timeline, click an annotation. To delete the annotation from the timeline, click **Delete**.

