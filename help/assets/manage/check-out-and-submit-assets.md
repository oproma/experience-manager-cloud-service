---
title: "Check In and Check Out Files in Assets"
description: "Learn how to check out assets for editing and check them back in after the changes are complete."
contentOwner: "AG"
sub-product: "AEM Assets as a Cloud Service"
user-guide-title: "AEM Assets as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/assets/check-out-and-submit-assets.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/assets/check-out-and-submit-assets.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/assets/check-out-and-submit-assets.md"
---
# Check-in and check-out files in Assets {#check-in-and-check-out-files-in-assets}

Adobe Experience Manager (AEM) Assets lets you check out assets for editing and check them back in after you complete making the changes. After you check out an asset, only you can edit, annotate, publish, move, or delete the asset. Checking out an asset locks the asset. Other users cannot perform any of these operations on the asset untill you check the asset back in to AEM Assets. However, they can still change the metadata for the locked asset.

To be able to check out/in assets, you require Write access on them.

This feature helps prevent other users from overriding the changes made by an author where multiple users collaborate on editing workflows across teams.

## Checking Out Assets {#checking-out-assets}

1. From the Assets UI, select the asset you want to check out. You can also select multiple assets to check out.

   ![chlimage_1-468](/help/assets/chlimage_1-468.png)

1. From the toolbar, click/tap the **Checkout** icon.

   ![chlimage_1-469](/help/assets/chlimage_1-469.png)

   Observe that the **Checkout** icon toggles to the **Checkin** icon with the lock open.

   ![chlimage_1-470](/help/assets/chlimage_1-470.png)

   To verify whether other users can edit the asset you checked out, log in as a different user. A lock icon appears on the thumbnail of the asset that you checked out.

   ![chlimage_1-471](/help/assets/chlimage_1-471.png)

   Select the asset. Notice that the toolbar does not display any options that let you edit, annotate, publish, or delete the asset.

   ![chlimage_1-472](/help/assets/chlimage_1-472.png)

   You can, however, click/tap the **View Properties** icon to edit the metadata for the locked asset.

1. Click/tap the Edit icon to open the asset in edit mode.

   ![chlimage_1-473](/help/assets/chlimage_1-473.png)

1. Edit the asset and save the changes. For example, crop the image and save.

   ![chlimage_1-474](/help/assets/chlimage_1-474.png)

   You can also choose to annotate or publish the asset.

1. Select the edited asset from the Assets UI, and click/tap the **Checkin** icon from the toolbar.

   ![chlimage_1-475](/help/assets/chlimage_1-475.png)

   The modified asset is checked in to AEM Assets and is available to other users for editing.

## Forced Check In {#forced-check-in}

Administrators can check in assets that are checked out by other users.

1. Log in to AEM Assets as an administrator.
1. From the Assets UI select one or more assets that have been checked out by other users.

   ![chlimage_1-476](/help/assets/chlimage_1-476.png)

1. From the toolbar, click/tap the **Release Lock** icon. The asset is checked back in and available for edit to other users.

   ![chlimage_1-477](/help/assets/chlimage_1-477.png)

