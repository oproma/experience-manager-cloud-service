---
title: "Review assets in folders and collections"
description: "Set up review workflows for assets within a folder or a collection and share it with reviewers or creative partners to seek feedback."
contentOwner: "AG"
sub-product: "AEM Assets as a Cloud Service"
user-guide-title: "AEM Assets as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/assets/bulk-approval.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/assets/bulk-approval.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/assets/bulk-approval.md"
---
# Review assets in folders and collections {#review-folder-assets-and-collections}

Using Adobe Experience Manager (AEM) Assets, you can set ad-hoc review workflows for assets that are in a folder or in a collection. You can share it with reviewers or creative partners to seek their feedback. You can either associate a review workflow with a project or create an independent review task.

After you share the assets, reviewers can approve or reject them. Notifications are sent at various stages of the workflow to notify intended recipients regarding the completion of various tasks. For example, when you share a folder or collection, the reviewer receives a notification that a folder/collection has been shared for review.

After the reviewer completes the review (approves or rejects assets), you receive a review completion notification.

## Creating a review task for folders {#creating-a-review-task-for-folders}

1. From the Assets user interface, select the folder for which you want to create a review task.
1. From the toolbar, tap/click the **Create Review Task** icon to open the **Review Task** page. If you cannot see the icon in the toolbar, tap/click **More** and then select the icon.

   ![chlimage_1-403](/help/assets/chlimage_1-403.png)

1. (Optional) From the **Project** list, select the project to which you want to associate the review task. By default, the **None** option is selected. If you do not want to associate any project with the review task, retain this selection.

   >[!NOTE]
   >
   >Only the projects for which you have Editor-level permissions (or higher) are visible in the **Projects** list.

1. Enter a name for the review task, and select an approver from the **Assign To** list.

   >[!NOTE]
   >
   >The members/groups of the selected project are available as approvers in the **Assign To** list.

1. Enter a description, the task priority, and the due date for the review task.

   ![task_details](/help/assets/task_details.png)

1. In the Advanced tab, enter a label to be used to create the URI.

   ![review_name](/help/assets/review_name.png)

1. Tap/click **Submit**, and then tap/click **Done** to close the confirmation message. A notification for the new task is sent to the approver.
1. Log in to AEM Assets as an Approver and navigate to the Assets UI. To approve assets, click/tap the **Notifications** icon and then select the review task from the list.

   ![notification](/help/assets/notification.png)

1. In the **Review Task** page, examine the details of the review task, and then tap/click **Review**.
1. In the **Review Task** page, select assets, and tap/click the **Approve/Reject** icon to approve or reject, as appropriate.

   ![review_task](/help/assets/review_task.png)

1. Tap/click the **Complete** icon from the toolbar. In the dialog, enter a comment and tap/click  **Complete** to confirm.
1. Navigate to the Assets UI, and open the folder. The approval status icons for the assets appear in both the Card and List views.

   **Card view**

   ![chlimage_1-404](/help/assets/chlimage_1-404.png)

   **List view**

   ![review_status_listview](/help/assets/review_status_listview.png)

## Creating a review task for collections {#creating-a-review-task-for-collections}

1. From the Collections page, select the collection for which you want to create a review task.
1. From the toolbar, tap/click the **Create Review Task** icon to open the **Review Task** page. If you cannot see the icon in the toolbar, tap/click **More** and then select the icon.

   ![chlimage_1-405](/help/assets/chlimage_1-405.png)

1. (Optional) From the **Project** list, select the project to which you want to associate the review task. By default, the **None** option is selected. If you do not want to associate any project with the review task, retain this selection.

   >[!NOTE]
   >
   >Only the projects for which you have Editor-level permissions (or higher) are visible in the **Projects** list.

1. Enter a name for the review task, and select an approver from the **Assign To** list.

   >[!NOTE]
   >
   >The members/groups of the selected project are available as approvers in the **Assign To** list.

1. Enter a description, the task priority, and the due date for the review task.

   ![task_details-collection](/help/assets/task_details-collection.png)

1. Tap/click **Submit**, and then tap/click **Done** to close the confirmation message. A notification for the new task is sent to the approver.
1. Log in to AEM Assets as an Approver and navigate to the Assets console. To approve assets, tap/click the **Notifications** icon and then select the review task from the list.
1. In the **Review Task** page, examine the details of the review task, and then tap/click **Review**.
1. All the assets in the collection are visible on the review page. Select the assets and tap/click the **Approve/Reject** icon to approve or reject assets, as appropriate.

   ![review_task_collection](/help/assets/review_task_collection.png)

1. Tap/click the **Complete** icon from the toolbar. In the dialog, enter a comment and tap/click **Complete** to confirm.
1. Navigate to the Collections console and open the collection. The approval status icons for the assets appear in both the Card and List views.

   **Card view**

   ![collection_reviewstatuscardview](/help/assets/collection_reviewstatuscardview.png)

   **List view**

   ![collection_reviewstatuslistview](/help/assets/collection_reviewstatuslistview.png)

