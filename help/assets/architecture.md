---
title: "Architecture of Adobe Experience Manager Assets as a Cloud Service"
description: "Architecture of Adobe Experience Manager Assets as a Cloud Service"
contentOwner: "AG"
sub-product: "AEM Assets as a Cloud Service"
user-guide-title: "AEM Assets as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/assets/architecture.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/assets/architecture.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/assets/architecture.md"
---
# Architecture of Assets as a Cloud Service solution {#assets-architecture}

AEM Assets as a Cloud Service is based on Adobe Experience Manager and compares well with the latest offering Experience Manager 6.5. The Cloud Service offering has an adjusted architecture and functionality offering to be a cloud-native service. Changes in the underlying architecture does not allow all Experience Manager 6.5 functionality to be a part of the Cloud Service.

To see architecture overview of asset microservices see this [asset microservices overview](/help/assets/asset-microservices-overview.md#asset-microservices-architecture).

>[!MORELIKETHIS]
>
>* [Overview and what's new](/help/assets/overview.md)
>* [Supported file formats and MIME types](/help/assets/file-format-support.md)
>* [Overview of asset microservices](/help/assets/asset-microservices-overview.md)

