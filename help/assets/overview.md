---
title: "Introduction to Assets as a Cloud Service"
description: "What's new in Assets as a Cloud Service."
contentOwner: "AG"
sub-product: "AEM Assets as a Cloud Service"
user-guide-title: "AEM Assets as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/assets/overview.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/assets/overview.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/assets/overview.md"
---
# Introducing Assets as a Cloud Service {#assets-cloud-service-introduction}

<!-- Need review information from gklebus -->

Adobe Experience Manager Assets as a Cloud Service offers a cloud-native, PaaS solution for businesses to not only perform their Digital Asset Management and Dynamic Media operations with speed and impact, but also use next-generation smart capabilities, such as AI/ML, from within a system that is always current, always available, and always learning.

Concurrent ingestion of many assets or complex assets is resource-intensive task for an AEM Author instance. The primary instance consumes considerable CPU, memory, and I/O resources when assets are added, processed, or even migrated. Such performance issues impact authoring and browsing experience of end users.

Businesses require support for a wide variety of file formats and content resolutions for multi-device, cross-geography, and multilingual use cases. Asset processing and storage requirements demand resources and capabilities that can overburden a traditional solution. At times, technical limitations of asset processing do not yield the desired results and at other times the cost of storage is an impediment for profit margins.

To begin with, understand the [benefits of a cloud-native offering](#solution-benefits). Check out the notable [changes to AEM as a Cloud Service](/help/release-notes/aem-cloud-changes.md) that also impact Experience Manager Assets followed up the notable [changes to Assets](/help/assets/assets-cloud-changes.md).

Read on to know the [details of the new Assets capabilities](#whats-new-assets) and the [known issues](/help/release-notes/known-issues.md). See a list of [deprecated or removed functionality](/help/release-notes/deprecated-removed-features.md) to know what is removed in this release and see this [list of upcoming capabilities](/help/release-notes/known-issues.md#upcoming-assets-capabilities) to know what's coming shortly. Finally, understand the AEM terms with the help of this [glossary](/help/overview/terminology.md).

## Solution benefits {#solution-benefits}

The following are the key benefits of Assets as a Cloud Service. To know more, see [overview of Experience Manager as a Cloud Service](/help/overview/introduction.md).

* **Modern Cloud Service for asset processing**: The new asset microservices is a cloud-based, scalable, reliable, and hassle-free asset processing service.
* **Highly scalable**: Elastic scalability across all types of deployments. Practically unlimited resources that are available on-demand, as and when needed. Saves the cost of over-design as compared to a traditional system.
* **Latest software**: Always current and always updated. All users have only the latest software with all patches, features, security, and bug fixes available to them. Developers and integrator work with the latest set of APIs without issues of multi-version support.
* **Always online**: Zero downtime (0dt), thanks to the instance deployed in a cluster with backups and redundancy. Upgrades are 0dt as well.
* **Constant monitoring**: The monitoring of the system is automated and built-in checks and triggers help maintain the performance, availability, and overall robustness.
* **Hassle-free deployments**: AEM in the Cloud operations are fully automated that require no manual intervention. For automated deployments, the Cloud Manager (CM) component automates the build of deployable Docker images containing your custom code.

## New Assets capabilities {#whats-new-assets}

The significant new capabilities are:

* [Asset microservices](/help/assets/asset-microservices-overview.md)
* [Asset upload methods](/help/assets/manage/add-assets.md)

