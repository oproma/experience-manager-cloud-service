---
title: "Publish assets, folders, and collections to Brand Portal"
description: "Publish assets, folders, and collections to Brand Portal."
contentOwner: "Vishabh Gupta"
sub-product: "AEM Assets as a Cloud Service"
user-guide-title: "AEM Assets as a Cloud Service"
product: "adobe experience manager"
git-repo: "https://github.com/AdobeDocs/experience-manager-cloud-service.en"
index: "y"
solution-title: "Adobe Experience Manager as a Cloud Service"
solution-hub-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/landing/home.html"
getting-started-title: "Getting Started"
getting-started-url: "https://docs.adobe.com/content/help/en/experience-manager-cloud-service/overview/home.html"
tutorials-title: "Tutorials"
tutorials-url: "https://docs.adobe.com/content/help/en/experience-manager-learn/cloud-service/overview.html"
git-commit: "3647715c2c2356657dfb84b71e1447b3124c9923"
last-update: "2020-05-06"
pipeline_filename: "/help/assets/publish-to-brand-portal.md"
git-commit-file: "3647715c2c2356657dfb84b71e1447b3124c9923"
git-edit: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/tree/master/help/assets/publish-to-brand-portal.md"
git-issue: "https://github.com/AdobeDocs/experience-manager-cloud-service.en/issues/new"
git-filename: "/help/assets/publish-to-brand-portal.md"
---
# Publish assets to Brand Portal {#publish-assets-to-brand-portal}

As an Adobe Experience Manager (AEM) Assets administrator, you can publish assets, folders, and collections to the AEM Assets Brand Portal instance. Also, you can schedule the publish workflow of an asset or folder to a later date or time. Once published, the Brand Portal users can access and further distribute the assets, folders, and collections to other users. 

However, you must first configure AEM Assets with Brand Portal. For details, see [Configure AEM Assets with Brand Portal](/help/assets/brand-portal/configure-aem-assets-with-brand-portal.md). 

If you make subsequent modifications to the original asset, folder, or collection in AEM Assets, the changes are not reflected in Brand Portal until you republish from AEM Assets. This feature ensures that work-in-progress changes are not available in Brand Portal. Only approved changes that are published by an administrator are available in Brand Portal.

* [Publish assets to Brand Portal](#publish-assets-to-bp)
* [Publish folders to Brand Portal](#publish-folders-to-brand-portal)
* [Publish collections to Brand Portal](#publish-collections-to-brand-portal)

>[!NOTE]
 >
 >Adobe recommends staggered publishing, preferably during non-peak hours, so that the AEM author does not occupy excess resources.
 >

## Publish assets to Brand Portal {#publish-assets-to-bp}

Following are the steps to publish assets from AEM Assets to Brand Portal:

1. From the Assets console, open the parent folder and select all the assets that you want to publish and click **Quick Publish** option from the toolbar.

   ![publish2bp-2](/help/assets/publish2bp.png)

1. Following are the two ways publish assets:
    * [Publish now](#publish-to-bp-now) (Publish assets immediately)
    * [Publish later](#publish-to-bp-later) (Schedule publishing assets)

### Publish assets now {#publish-to-bp-now}

To publish the selected assets to Brand Portal, do either of the following:

* From the toolbar, select **Quick Publish**. Then from the menu, click **Publish to Brand Portal**.

* From the toolbar, select **Manage Publication**.

  1. From **Action**, select **Publish to Brand Portal**. 
  
     From **Scheduling**, select **Now**. 
     
     Click **Next**.

  2. Confirm your selection in **Scope** and click **Publish to Brand Portal**.

A message appears stating that the assets have been queued up for publishing to Brand Portal. Login to the Brand Portal interface to see the published assets.

### Publish assets later {#publish-to-bp-later}

To schedule publishing the assets to Brand Portal to a later date or time:

1. Select the assets that you want to schedule for publishing and click **Manage Publication** from the tool bar at the top.
  
1. On **Manage Publication** page, select **Publish to Brand Portal** from **Action**.

   Select **Later** from **Scheduling**.

    ![publishlaterbp-1](/help/assets/publishlaterbp-1.png)

1. Select an **Activation date** and specify time. Click **Next**.

1. Select an **Activation date** and specify time. Click **Next**.

1. Specify a **Workflow title** in **Workflows**. Click **Publish Later**.

    ![publishworkflow](/help/assets/publishworkflow.png)

Login to the Brand Portal interface to see the published assets (depending on your scheduled date or time).

   ![bp_landing_page](/help/assets/bp_landing_page.png)


## Publish folders to Brand Portal {#publish-folders-to-brand-portal}

You can publish or unpublish asset folders immediately, or schedule to a later date or time.

### Publish folders to Brand Portal {#publish-folders-to-bp}

1. From the Assets console, select the folders that you want to publish and click **Quick Publish** option from the toolbar.

   ![publish2bp](/help/assets/publish2bp.png)

1. **Publish folders now**

   To publish the selected folders to Brand Portal, do either of the following:

    * From the toolbar, select **Quick Publish**. 
    
      From the menu, select **Publish to Brand Portal**.

    * From the toolbar, select **Manage Publication**.

      1. From **Action**, select **Publish to Brand Portal**.
      
         From **Scheduling**, select **Now**.
         
         Click **Next.**

      1. Confirm your selection in **Scope** and click **Publish to Brand Portal**.

   A message appears stating that the folder has been queued up for publishing to Brand Portal. Login to your Brand Portal interface to see the published folder.

1. **Publish folders later**

   To schedule the publishing the asset folders to a later date or time:

    1. Select the folders that you want to schedule for publishing, select **Manage Publication** from the tool bar at the top.
    1. From **Action**, select **Publish to Brand Portal**.
    
       From **Scheduling**, select **Later**.

   1. Select an **Activation date** and specify time. Click **Next**.

       ![publishlaterbp](/help/assets/publishlaterbp.png)
    
    1. Confirm your selection in **Scope**. Click **Next**.

    1. Specify a Workflow title under **Workflows**. Click **Publish Later**.

       ![manageschedulepub](/help/assets/manageschedulepub.png)

### Unpublish folders from Brand Portal {#unpublish-folders-from-brand-portal}

You can remove any asset folder published to Brand Portal by unpublishing it from AEM Assets instance. After you unpublish the original folder, its copy is no longer available to Brand Portal users.

You can unpublish asset folders from Brand Portal immediately, or schedule to a later date and time. 

To unpublish asset folders from Brand Portal:

1. From the Assets console, select the asset folders that you want to upublish and click **Manage Publication** option from the toolbar. 

   ![publish2bp-1](/help/assets/publish2bp.png)

1. **Unpublish asset folders now**

   To immediately unpublish the selected asset folder from Brand Portal:

    1. From the toolbar, select **Manage Publication**.

    1. From **Action** select **Unpublish from Brand Portal**. 
    
       From **Scheduling**, select **Now**.
       
       Click **Next**.

    1. Confirm your selection in **Scope** and click **Unpublish from Brand Portal**.

       ![confirm-unpublish](/help/assets/confirm-unpublish.png)

1. **Unpublish asset folders later**

   To schedule unpublishing of an asset folder to a later date and time:

    1. From the toolbar, select **Manage Publication**.
    
    1. From **Action**, select **Unpublish from Brand Portal**.
    
       From **Scheduling**, select **Later**.

    1. Select an **Activation date** and specify the time. Click **Next**.

    1. Confirm your selection in **Scope** and click **Next**.

    1. Specify a **Workflow title** in **Workflows**. Click **Unpublish Later**.

       ![unpublishworkflows](/help/assets/unpublishworkflows.png)

## Publish collections to Brand Portal {#publish-collections-to-brand-portal}

You can publish or unpublish collections from your AEM Assets cloud instance.

>[!NOTE]
 >
 >Content fragments cannot be published to the Brand Portal. Therefore, if you select content fragment(s) in AEM Assets, then **Publish to Brand Portal** action is not available.
 >
 >If collections containing content fragments are published from AEM Assets to Brand Portal, then all the contents of the folder except content fragments is replicated to the Brand Portal interface.
 >

### Publish collections {#publish-collections}

Following are the steps to publish collections from AEM Assets to Brand Portal:

1. In the AEM Assets UI, click AEM logo. 

1. From **Navigation** page, go to **Assets** > **Collections**.

1. From the **Collections** console, select the collections that you want to publish to Brand Portal.

   ![select_collection](/help/assets/select_collection.png)

1. From the toolbar, click **Publish to Brand Portal**.

1. In the confirmation dialog, click **Publish**.

1. Close the confirmation message.

   Login to Brand Portal as an administrator. The published collection is available in the Collections console.

   ![published collection](/help/assets/published_collection.png)

### Unpublish collections {#unpublish-collections}

You can remove any collection published to Brand Portal by unpublishing it from your AEM Assets instance. After you unpublish the original collection, its copy is no longer available to the Brand Portal users.

Following are the steps to unpublish a collection:

1. From the **Collections** console of your AEM Assets instance, and select the collection that    you want to unpublish.

   ![select_collection](/help/assets/select_collection-1.png)

1. From the toolbar, click **Remove from Brand Portal** icon.
1. In the dialog, click **Unpublish**.
1. Close the confirmation message. The collection is removed from the Brand Portal interface.

In addition to the above, you can also publish metadata schemas, image presets, search facets, and tags from AEM Assets to Brand Portal. 

* [Publish presets, schemas, and facets to Brand Portal](https://experienceleague.adobe.com/docs/experience-manager-brand-portal/using/publish/publish-schema-search-facets-presets.html)
* [Publish tags to Brand Portal](https://experienceleague.adobe.com/docs/experience-manager-brand-portal/using/publish/brand-portal-publish-tags.html)


See, [Brand Portal documentation](https://experienceleague.adobe.com/docs/experience-manager-brand-portal/using/home.html) for more information.


<!--
   Comment Type: draft

   <li> </li>
   -->

   <!--
   Comment Type: draft

   <li>Step text</li>
   -->

